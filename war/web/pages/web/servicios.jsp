<%@page import="com.locuiuris.vo.ServicioWebVO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.locusiuris.utils.StringKeys"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<!DOCTYPE html>
<html lang="es">
  <head>
      <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="DC.Title" content="Abogados Oviedo" />
	<meta http-equiv="title" content="Abogados Oviedo" />
	<meta http-equiv="description" content="Despacho de abogados en Oviedo" />
	<meta http-equiv="DC.Description" content="Despacho de abogados en Oviedo" />
	<meta name="keywords" content="abogados, oviedo, Villaviciosa de Asturias, abogados laboralistas, multas, tráfico, trafico" />
	<meta name="REVISIT" content="2 days" />
	<meta name="REVISIT-AFTER" content="2 days" />
	<meta name="audience" content="Alle" />
	<meta name="classification" content="web" />
	<meta name="rating" content="general"/> 
	<meta name="Robots" content="Index, Follow" />
	<meta name="language" content="ES" />
	<meta name="DC.Language" scheme="RFC1766" content="Spanish" />
	<meta name="VW96.objecttype" content="Document" />
	<meta name="distribution" content="global" />
	<meta name="resource-type" content="document" />
	<meta http-equiv="Last-Modified" content="0" />
	<meta http-equiv="Cache-Control" content="no-cache, must-revalidate" />
	<meta http-equiv="Content-Language" content="es" />
	<meta name="author" content="Alberto Marqués">
    <link rel="shortcut icon" href="../../assets/ico/favicon.png">

    <title>ALVAREZ ABOGADOS</title>

    <!-- Bootstrap core CSS -->
     
<link href="./dist/css/bootstrap.css" rel="stylesheet">

<link href="./dist/css/bootstrap-theme.css" rel="stylesheet">
    <link href="./css/locusweb.css" rel="stylesheet">
	<link href="./css/carousel.css" rel="stylesheet">
	<style type="text/css">
      html { height: 100% }
      body { height: 100%; margin: 0; padding: 0 }
      h2 {color: #ffcc33;}
      h3 {color: #ffcc33;}
       
   
    </style>
    <!-- Custom styles for this template 
    <link href="./css/offcanvas.css" rel="stylesheet">
    <script src="../../assets/js/html5shiv.js"></script>
    <script src="../../assets/js/respond.min.js"></script>-->
    <script src="./js/jquery.js"></script>
   <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false">
    </script>
	<script type="text/javascript" src="js/home.js"></script>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
   
  </head>

  <body >
 <div id="fb-root"></div>
	<script src="./js/facebook.js"></script>
	<div class="navbar navbar-fixed-top" role="navigation">
		<div class="navbar-inner">
			<div class="container alturamenu">
				<div class="row alturamenu1">
					<div class=".col-xs-6 .col-lg-4"></div>
					<div class=".col-xs-12 .col-lg-8 ">
						<div class="sf-menu collapse navbar-collapse ">
						<br>
							<ul class="nav navbar-nav ">
								<li ><a href="./welcome">INICIO</a></li>
								<li ><a href="./quien.jsp">QUIENES SOMOS</a></li>
								<li class="active"><a
									href="./servicios.jsp">SERVICIOS</a></li>
								<li><a href="consulta.jsp">CONSULTAS</a></li>
								<li><a
									href="../../../blogservlet?operacion=<%=com.locusiuris.utils.StringKeys.LISTAENTRADAS%>&sitioweb=1">BLOG</a></li>
								<li><a href="donde.jsp">DONDE ESTAMOS</a></li>
								<li>
								<div class="fb-like" layout="button_count"
										data-href="https://www.facebook.com/pages/Alvarezabogadosdespacho/526673600750646"
										data-width="100" data-show-faces="false" data-send="false" style="margin-top: 16px;"></div>&nbsp;<a href="https://twitter.com/alvarezdespacho"
									class="twitter-follow-button" data-show-count="true"
									data-lang="es" data-dnt="false"  >Seguir</a></li>
								<li><br>
								<div style="margin-top: 1px;">
								</div></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-md-4 col-lg-3">
						<div class="navbar-header">
						<a class="navbar-brand" href="index.jsp"><img src="../../img/logo.png" style="height: 50px;"/></a>
							<button type="button" class="btn-custom navbar-toggle"
								data-toggle="collapse" data-target=".navbar-collapse">
								<span>Menu</span>
							</button>
							
						</div>
					</div>
					<div class="hidden-xs col-md-4 col-lg-5 "><br><div class="tel"><p><img alt="" src="./img/icon_mail.png" class="iconocontacto" >&nbsp;abogadosagb@gmail.com</p></div> </div>
					<div class="hidden-xs col-md-4 col-lg-4 "><br><div class="tel"><p><img alt="" src="./img/telefono.png" class="iconocontacto" >&nbsp;984.127.087</p></div> </div>
				</div>
			</div>
		</div>
	</div>
	<!-- /.navbar -->

    <div class="container">
    	<br>   <br>     
     <div class="row">
                    <div class="col-xs-12 col-sm-8 col-lg-8">
                        <fieldset><!-- Text input-->
                        <div class="control-group">
                          <label class="control-label"></label>
                           <div class="controls">
	                          <form class="form-horizontal" action="./servicios.jsp" method="get" >
		                            <input id="buscar" name="buscar" type="text" placeholder="Busqueda *" class="input-xlarge" required>
		                            <input type="submit" class="btn btn-info btn-small active" value="Buscar" style="margin-top: 20px;margin-bottom: 20px;"/>
		                       </form>
	                       </div>
                        </div>
                        </fieldset>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-lg-4 textocapitulo" ><h2>Servicios que ofrecemos</h2></div>
                  </div>
		<br>   <br>   
		<%
   			List<ServicioWebVO> oListServicios = null;
   			String sHtml = "";
   			int i = 0;
   			if (request.getSession().getAttribute(StringKeys.LISTASERVICIOS) != null){
   			    oListServicios = (List<ServicioWebVO>) request.getSession().getAttribute(StringKeys.LISTASERVICIOS);
   		              
   			        		if (request.getParameter("buscar")!=null){
   						String sCadenaBusqueda = request.getParameter("buscar");
   						List<ServicioWebVO> oListBusquedas = new ArrayList();
   						
   						for (ServicioWebVO servicio : oListServicios){
   							if ((servicio.getsTitulo()!=null&&servicio.getsContenido()!=null)
   									&& (servicio.getsTitulo().contains(sCadenaBusqueda)||servicio.getsContenido().contains(sCadenaBusqueda)))
   								oListBusquedas.add(servicio);
   						}
   						oListServicios = oListBusquedas; 
   					}
   				}
   		              	
   		             	String sEntrada = "<div class=\"row\">"+
   		              	"<div class=\"col-6 col-sm-6 col-lg-4\">"+
   		                "<h3>Titulo</h3>"+
   		                "<p>Contenido </p>"+
   		                "<p><a class=\"btn btn-default\" href=\"../../../servicioservlet?operacion="+StringKeys.DETALLESERVICIO+"&key=CLAVESERVICIO\"style=\"color:#ff9999;\" style=\"color:#ff9999;\" ><b>Ver Detalles &raquo;</b></a></p>"+
   		              	"</div>";
   		              
   		              	
   			  	for (ServicioWebVO servicio:oListServicios){
   			  			sEntrada = sEntrada.replace("Titulo", servicio.getsTitulo());
   			  			sEntrada = sEntrada.replace("Contenido", ""+servicio.getsContenido());
   			  			sEntrada = sEntrada.replace("CLAVESERVICIO",
   								"" + servicio.getKey());
   			  			sHtml = sHtml + sEntrada;
   			  			sEntrada = ""+
   			              	"<div class=\"col-6 col-sm-6 col-lg-4\">"+
   			                "<h3>Titulo</h3>"+
   			                "<p>Contenido </p>"+
   			                "<p><a class=\"btn btn-default\" href=\"../../../servicioservlet?operacion="+StringKeys.DETALLESERVICIO+"&key=CLAVESERVICIO\"style=\"color:#ff9999;\" style=\"color:#ff9999;\" ><b>Ver Detalles &raquo;</b></a></p>"+
   			              	"</div>";
   			             i++;
   			  	}
   			  	sHtml = sHtml + "</div>"; 
   			  	out.println(sHtml);
   		%>   

          
       <br>   <br>   <br>

      <hr>

  	<jsp:include page="./includes/footer.jsp" />

    </div><!--/.container-->
	
	
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../../assets/js/jquery.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>
    <script src="offcanvas.js"></script>
   <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-44892985-1', 'alvarezdespacho.appspot.com');
  ga('send', 'pageview');

</script>
  </body>
</html>
