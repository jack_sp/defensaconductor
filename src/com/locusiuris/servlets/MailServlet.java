package com.locusiuris.servlets;

import java.io.IOException;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;


public class MailServlet  extends HttpServlet{
	
	private static final Logger log = Logger.getLogger(MailServlet.class.getName());
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		String sUrl = "./consulta.jsp?";
		String sResultado = "mail=ok";
		String sEmailConsulta = req.getParameter("email");
		String sCiudad = "";
		String sTelefono = "";
		if (req.getParameter("ciudad")!=null)
			sCiudad = req.getParameter("ciudad");
		if (req.getParameter("telefono")!=null)
			sTelefono = req.getParameter("telefono");
		
		String msgBody = " Contestar a: "+sEmailConsulta+"\n"+req.getParameter("consulta");
		
		  Properties props = new Properties();
	        Session session = Session.getDefaultInstance(props, null);


	        try {
	            Message msg = new MimeMessage(session);
	            msg.setFrom(new InternetAddress("abogados.agb@gmail.com", "Correo Admin Alvarez Abogados"));
	            msg.addRecipient(Message.RecipientType.CC,
                        new InternetAddress(sEmailConsulta, "Consulta"));
	            msg.addRecipient(Message.RecipientType.TO,
	                             new InternetAddress("info@defensadelconductor.com", "Consulta de "+ sEmailConsulta));
	            msg.setSubject("Consulta web alvarez abogados");
	            msg.setText(msgBody);
	            Transport.send(msg);
	    
	        } catch (AddressException e) {
	        	sResultado = "mail=error";
	        	log.error("log error "+e.getMessage());
	        } catch (MessagingException e) {
	        	sResultado = "mail=error";
	        	log.error("log error "+e.getMessage());
	        } catch (Exception e) {
	        	sResultado = "mail=error";
	        	log.error("log error "+e.getMessage());
	        }
	        
        sUrl = sUrl + sResultado;
        
		resp.sendRedirect(sUrl);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String sUrl = "./consulta.jsp?";
		String sResultado = "mail=ok";
		String sEmailConsulta = req.getParameter("email");
		String sCiudad = "";
		String sTelefono = "";
		if (req.getParameter("ciudad")!=null)
			sCiudad = req.getParameter("ciudad");
		if (req.getParameter("telefono")!=null)
			sTelefono = req.getParameter("telefono");
		
		String msgBody = " Contestar a: "+sEmailConsulta+"\n"+req.getParameter("consulta");
		
		  Properties props = new Properties();
	        Session session = Session.getDefaultInstance(props, null);


	        try {
	            Message msg = new MimeMessage(session);
	            msg.setFrom(new InternetAddress("abogados.agb@gmail.com", "Correo Admin Alvarez Abogados"));
	            msg.addRecipient(Message.RecipientType.CC,
                        new InternetAddress(sEmailConsulta, "Consulta"));
	            msg.addRecipient(Message.RecipientType.TO,
	                             new InternetAddress("info@defensadelconductor.com", "Consulta de "+ sEmailConsulta));
	            msg.setSubject("Consulta web alvarez abogados");
	            msg.setText(msgBody);
	            Transport.send(msg);
	    
	        } catch (AddressException e) {
	        	sResultado = "mail=error";
	        	log.error("log error "+e.getMessage());
	        } catch (MessagingException e) {
	        	sResultado = "mail=error";
	        	log.error("log error "+e.getMessage());
	        } catch (Exception e) {
	        	sResultado = "mail=error";
	        	log.error("log error "+e.getMessage());
	        }
	        
        sUrl = sUrl + sResultado;
        
		resp.sendRedirect(sUrl);
	}

}
