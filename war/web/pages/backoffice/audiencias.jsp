<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Audiencias</title>
	<!-- Bootstrap core CSS -->
    <link rel="stylesheet" type="text/css"
		href="../../bootstrap2/lib/css/bootstrap.min.css"></link>
	<link rel="stylesheet" type="text/css"
		href="../../bootstrap2/lib/css/prettify.css"></link>
    <link type="text/css" rel="stylesheet" href="../../dist/css/displaytag.css"/>

    <!-- Custom styles for this template -->
    <link href="navbar.css" rel="stylesheet">
    
<style type="text/css">
html, body,  iframe { margin:0; padding:0; height:98%;margin-top: 2%; }
iframe { display:block; width:100%; border:none; }
</style>
</head>
<body>
 <div class="navbar navbar-inverse navbar-fixed-top">
			<div class="navbar-inner">
				<div class="container">
					<button type="button" class="btn btn-navbar" data-toggle="collapse"
						data-target=".nav-collapse">
						<span class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<a class="brand" href="./index.jsp">Back Office</a>
					<div class="nav-collapse collapse">
						<ul class="nav">
							<li ><a href="./blog.jsp">Blog</a></li>
							<li class="active"><a href="./servicios.jsp">Servicios</a></li>
							<li><a href="#">Carrusel</a></li>
							<li><a href="https://www.google.com/analytics/web/?et&authuser=0#home/a590466w986427p970264/">Audiencias</a></li>
							<li><a href="#">Ayuda</a></li>
						</ul>
					</div>
					
				</div>
			</div>
		</div>
		  </div> 
 <!--
  <div class="container">

       Static navbar --><!--
        <div class="navbar navbar-inverse navbar-fixed-top">
			<div class="navbar-inner">
				<div class="container">
					<button type="button" class="btn btn-navbar" data-toggle="collapse"
						data-target=".nav-collapse">
						<span class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<a class="brand" href="./index.jsp">Back Office</a>
					<div class="nav-collapse collapse">
						<ul class="nav">
							<li ><a href="./blog.jsp">Blog</a></li>
							<li class="active"><a href="./servicios.jsp">Servicios</a></li>
							<li><a href="#">Carrusel</a></li>
							<li><a href="https://www.google.com/analytics/web/?et&authuser=0#home/a590466w986427p970264/">Audiencias</a></li>
							<li><a href="#">Ayuda</a></li>
						</ul>
					</div>
					
				</div>
			</div>
		</div>
		  </div> 
		<br><br>

       Main component for a primary marketing message or call to action 
      <div class="jumbotron">
        
        <iframe src="https://www.google.com/analytics/web/?et&authuser=0#home/a590466w986427p970264/"></iframe>
        
      </div>-->

  <iframe src="http://google.com/"></iframe>


   <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../../bootstrap2/lib/js/wysihtml5-0.3.0.js"></script>
	<script src="../../bootstrap2/lib/js/jquery-1.7.2.min.js"></script>
	<script src="../../bootstrap2/lib/js/prettify.js"></script>
	<script src="../../bootstrap2/lib/js/bootstrap.min.js"></script>
	<script src="../../dist/js/bootstrap-wysihtml5.js"></script>
</body>
</html>