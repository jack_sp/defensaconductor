package com.locusiuris.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.Text;
import com.locuiuris.vo.EntradaWebVO;
import com.locuiuris.vo.ServicioWebVO;
import com.locusiuris.db.BlogPostDB;
import com.locusiuris.db.ServiciosDB;
import com.locusiuris.utils.StringKeys;

@SuppressWarnings("serial")
public class ServicioServlet extends HttpServlet {
	
	private List<EntradaWebVO> getEntradas(BlogPostDB oBlogPostDB){
		List<EntradaWebVO> oListEntradas = oBlogPostDB.listadoPost();
		List<EntradaWebVO> oListSession = new ArrayList<EntradaWebVO>();
		for (EntradaWebVO oEntradaVO :oListEntradas){
			oListSession.add(oEntradaVO);
		}
		return oListSession;
	}
	
	private List<ServicioWebVO> getServicios(ServiciosDB oServiciosDB){
		List<ServicioWebVO> oListServicios = oServiciosDB.listadoServicio();
		List<ServicioWebVO> oListSession = new ArrayList<ServicioWebVO>();
		for (ServicioWebVO oServicioVO :oListServicios){
			oListSession.add(oServicioVO);
		}
		return oListSession;
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		String sCadena = (String) req.getParameter(StringKeys.OPERACION);
		String sSitioWeb = "backoffice";
		try{
		if (((String) req.getParameter(StringKeys.SITIOWEB)).equals("1"))
			sSitioWeb = "web";
		}catch(Exception e){
			
		}
		ServiciosDB oServiciosDB = new ServiciosDB();
	//	String sUrl = "./web/pages/"+sSitioWeb+"/servicios.jsp";
		String sUrl = "./servicios.jsp";
		BlogPostDB oBlogPostDB = new BlogPostDB();
		
		if (sCadena.equals(StringKeys.LISTASERVICIOS)){			
			List<ServicioWebVO> oListSession  = getServicios(oServiciosDB);
			
			try{
				if (((String) req.getParameter(StringKeys.DESDEINDEX)).equals("1")){
				//	sUrl = "./web/pages/"+sSitioWeb+"/index.jsp";
					sUrl = "./welcome";
					List<EntradaWebVO> oListEntradas  = getEntradas(oBlogPostDB);
					req.getSession().setAttribute("listaentradas", oListEntradas);
				}
			}catch(Exception e){}
			req.getSession().setAttribute(StringKeys.LISTASERVICIOS, oListSession);
		}else if (sCadena.equals(StringKeys.BORRADO)){
			String sKey = (String) req.getParameter(StringKeys.KEY);
			oServiciosDB.borradoServicio(sKey);
			
			List<ServicioWebVO> oListSession  = getServicios(oServiciosDB);
			req.getSession().setAttribute(StringKeys.LISTASERVICIOS, oListSession);
			
		}else if (sCadena.equals(StringKeys.EDITADO)){
			String sKey = (String) req.getParameter(StringKeys.KEY);
			ServicioWebVO oServicioVO =oServiciosDB.consultaServicio(sKey);
			
			req.getSession().setAttribute(StringKeys.SERVICIOVO, oServicioVO);
			sUrl =	"./web/pages/backoffice/editServicio.jsp?key="+req.getParameter("key");
		}else if (sCadena.equals(StringKeys.DETALLESERVICIO)){
			String sKey = (String) req.getParameter(StringKeys.KEY);
			ServicioWebVO oServicioVO =oServiciosDB.consultaServicio(sKey);
			
			req.getSession().setAttribute(StringKeys.SERVICIOVO, oServicioVO);
			sUrl =	"./detalleServicio.jsp?key="+req.getParameter("key");
		}
		resp.sendRedirect(sUrl);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String sCadena = (String) req.getParameter(StringKeys.OPERACION);
		ServiciosDB oServiciosDB = new ServiciosDB();
		ServicioWebVO oServicioVO = new ServicioWebVO();
		oServicioVO.setsContenido(new Text((String) req.getParameter(StringKeys.NOTATEXTAREA)));
		oServicioVO.setsTitulo((String) req.getParameter(StringKeys.TITULO));
		oServicioVO.setsServicio((String) req.getParameter(StringKeys.SERVICIO));
		oServicioVO.setsResumen((String) req.getParameter(StringKeys.RESUMEN));
		String sUrl = "./web/pages/backoffice/servicios.jsp";
		
		if (sCadena.equals(StringKeys.ALTASERVICIO)){
			oServiciosDB.guardarServicio(oServicioVO);
		}else if (sCadena.equals(StringKeys.EDITARSERVICIO)){
			String sId = (String) req.getParameter(StringKeys.KEY);
			oServiciosDB.actualizaServicio(sId,oServicioVO);
		}
		List<ServicioWebVO> oListSession  = getServicios(oServiciosDB);
		req.getSession().setAttribute(StringKeys.LISTASERVICIOS, oListSession);
		
		resp.sendRedirect(sUrl);
	}
	
}
