<!DOCTYPE html>
<%@page import="com.locuiuris.vo.ServicioWebVO"%>
<%@page import="com.locuiuris.vo.EntradaWebVO"%>
<%@page import="com.locusiuris.utils.StringKeys"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html lang="en">
 <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.png">

    <title>BackOffice Alvarez Abogados</title>

	<!-- Custom styles for this template -->
	<link href="navbar.css" rel="stylesheet">
	
	
	
	
	
	<!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
	<!--[if lt IE 9]>
	        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	        <![endif]-->
	
	
	<link rel="stylesheet" type="text/css"
		href="../../bootstrap2/lib/css/bootstrap.min.css"></link>
	<link rel="stylesheet" type="text/css"
		href="../../bootstrap2/lib/css/prettify.css"></link>
	<link rel="stylesheet" type="text/css"
		href="../../dist/css/bootstrap-wysihtml5.css"></link>
	
	<script type="text/javascript">
		function seleccionaServicio() {
			var servicio = document.getElementById("servicio");
			var servicioEntrada = document.getElementById("serviciorequest").value;
			var i = 0;
			while (i <= servicio.options.length
					&& servicioEntrada.indexOf(servicio.options[i].value) == -1) {
				i++;
			}
			servicio.options[i].selected = true;
		}
		
		function activar(){
			$('#servicio').hide();
			$('#servicio').val("desactivado");
			$('#nuevaCategoria').removeAttr('disabled');
		}
	</script>
  </head>

<body >
	

    <div class="container">

      <div class="navbar navbar-inverse navbar-fixed-top">
			<div class="navbar-inner">
				<div class="container">
					<button type="button" class="btn btn-navbar" data-toggle="collapse"
						data-target=".nav-collapse">
						<span class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<a class="brand" href="./index.jsp">Back Office</a>
					<div class="nav-collapse collapse">
						<ul class="nav">
							<li><a href="./blog.jsp">Blog</a></li>
							<li class="active"><a href="./servicios.jsp">Servicios</a></li>
							<li><a href="#">Carrusel</a></li>
							<li><a href="https://www.google.com/analytics/web/?et&authuser=0#home/a590466w986427p970264/" target="_blank">Audiencias</a></li>
							<li><a href="#">Ayuda</a></li>
						</ul>
					</div>
					<!--/.nav-collapse -->
				</div>
			</div>
		</div>
<br>
		<br>

      <!-- Main component for a primary marketing message or call to action -->
      <div class="jumbotron">
        
        
        
       
        
        <form method="post" action="../../../blogservlet" id="form1">
        
              
               
            <input type='hidden' id="operacion" name="operacion" value="altapost">
            
                <div class="row-fluid">
                    <div class="span4"><legend>Titulo</legend>
                          <input type='text' id="titulo" name="titulo" ></div>
                    <div class="span4"><legend>Resum&eacute;n</legend>
						<textarea id="resumen" name="resumen" placeholder="Inserta texto aqu&iacute; (M&aacute;ximo 120 caracteres)"
							style="width: 250px; height: 90px" maxlength="120"  cols=36 rows=4 ></textarea></div>
                    <div class="span4"> <legend>Categor&iacute;a</legend>
                          <select id="servicio" name="servicio" >
                          	<% java.util.List<String> oListCategorias = (java.util.List<String>)request.getSession().getAttribute(""+StringKeys.LISTACATEGORIAS); 
                          		if (oListCategorias!=null)
	                          		for(String sCadena :oListCategorias)
	                          			out.println("<option value="+sCadena+">"+sCadena+"</option>");
                          	%>
                            </select>
                            
                            <input  id="nuevaCategoria" name="nuevaCategoria" maxlength="100" disabled/>
                            
                            <a href="#" class="btn btn-success btn-large" onclick="activar();" id="botonactivar"><i class="icon-white icon-plus-sign"></i> Añadir nueva categor&iacute;a</a>
                        
                    </div>
              	</div>
              	 <div class="row-fluid">
					<div class="span12">
						<legend>Contenido</legend>
						
						<textarea id="notatextarea" name="notatextarea" class="textarea"
							style="width: 810px; height: 200px" cols=110 rows=6></textarea>
					</div>
				</div>
           <div class="row-fluid">
					<div class="span6">
						<input type='submit' class="btn btn-lg btn-primary">
					</div>
					<div class="span6">
						<input id="checkbox1" type="checkbox" value="1" checked> <span id="label">Facebook</span></input>
						<input id="checkbox2" type="checkbox" value="1" checked> <span id="label">Twiter</span></input>
						<input id="checkbox3" type="checkbox" value="1" checked> <span id="label">Linkedin</span></input>
					</div>
				</div>
               

			</form>
        
        
      </div>

   </div><!-- /container -->

	<script src="../../bootstrap2/lib/js/wysihtml5-0.3.0.js"></script>
	<script src="../../bootstrap2/lib/js/jquery-1.7.2.min.js"></script>
	<script src="../../bootstrap2/lib/js/prettify.js"></script>
	<script src="../../bootstrap2/lib/js/bootstrap.min.js"></script>
	<script src="../../dist/js/bootstrap-wysihtml5.js"></script>

	<script>
		$('.textarea').wysihtml5();
	</script>

	<script type="text/javascript" charset="utf-8">
		$(prettyPrint);
	</script> 
  </body>
</html>