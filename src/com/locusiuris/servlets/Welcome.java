package com.locusiuris.servlets;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



public class Welcome extends HttpServlet {
	
	private static final Logger log = Logger.getLogger(ServicioServlet.class.getName());
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

	/*	String sLista = com.locusiuris.utils.StringKeys.LISTASERVICIOS;
		String sLista2 = com.locusiuris.utils.StringKeys.LISTAENTRADAS;
		String sParametro = com.locusiuris.utils.StringKeys.DESDEINDEX;
		try {
			if ((Object) req.getSession().getAttribute(sLista) == null)
				resp.sendRedirect("../../../servicioservlet?operacion="
						+ sLista + "&sitioweb=1&" + sParametro + "=1");
		} catch (Exception e) {
			log.severe(" Welcome >>>>>>>>>> "+e.getMessage());
		}*/
		RequestDispatcher rd = req
				.getRequestDispatcher("./index.jsp");
		rd.forward(req, resp);
	}
}
