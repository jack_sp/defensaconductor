<%@page import="com.locuiuris.vo.ServicioWebVO"%>
<%@page import="com.locuiuris.vo.EntradaWebVO"%>
<%@page import="java.util.List"%>
<%@page import="com.locusiuris.utils.StringKeys"%>
<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html lang="es">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="../../assets/ico/favicon.png">

<title>ALVAREZ ABOGADOS</title>


<link href="../../dist/css/bootstrap.css" rel="stylesheet">
<link href="../../dist/css/bootstrap-theme.css" rel="stylesheet">
<link href="./css/locusweb.css" rel="stylesheet">
<link href="./css/carousel.css" rel="stylesheet">

<style type="text/css">
html {
	height: 100%
}

body {
	height: 100%;
	margin: 0;
	padding: 0;
}

h3 {
	color: #ffcc33;
}
</style>
<!-- Custom styles for this template -->
<link href="./css/offcanvas.css" rel="stylesheet">

	
<script src="../../assets/js/html5shiv.js"></script>
<script src="../../assets/js/respond.min.js"></script>
<script src="js/jquery.js"></script>

<script type="text/javascript"
	src="http://maps.googleapis.com/maps/api/js?sensor=false">
	
</script>
<script type="text/javascript" src="js/home.js"></script>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->

</head>

<body>
	<div id="fb-root"></div>
	<script src="./js/facebook.js"></script>
	<script src="./js/validacionesFormularios.js"></script>
	<script src="./dist/js/bootstrap.js"></script>
		<script src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
	<!-- jquery api cargando y datos sobre el panel de resultados
		


	<script src="http://jquerymobile.com/demos/1.2.0/docs/_assets/js/jqm-docs.js"></script>
	<script src="http://jquerymobile.com/demos/1.2.0/js/jquery.mobile-1.2.0.js"></script> -->
	<!-- jquery api cargando y datos sobre el panel de resultados -->
	
	<script>
	
		function registrarCliente(email,tlf){
			var email = $('#emailaviso').val();
			var tlf = $('#tlfnaviso').val();
			var matricula = $('#matricula').val();
			var nif = $('#NIF').val();
			
			var sUrlBusqueda =  "http://multasradar.appspot.com/rest/clientes?operacion=addcliente&email="+email+"&tlf="+tlf+"&matricula="+matricula+"&nif="+nif;
			
				$.getJSON(sUrlBusqueda,
						function(data){
					$(".resultadosmultas").append("<div  class=\"resultadosContainer alert alert-success\" ><a class=\"close\" data-dismiss=\"alert\">×</a>Tu email y n&uacute;mero ha quedado guardado en nuestra base de datos </div>");
				}); 
		}
	
		function cargaMultas(nif,matricula,sResultados){
			var sUrlBusqueda =  "http://multasradar.appspot.com/rest/multas?nif="+nif+"&matricula="+matricula;
			
			$(".resultadosContainer").remove();
			$("#googleplay").remove();
			$.getJSON(sUrlBusqueda,
					function(data){
						var sCadena = "<div class=\"resultadosContainer\">";
						
						var cont = 0;
						var sImporte = 0;
					
						$.each(data, function(i,data){
							var urlDisposicion 	=  	data.urlDisposicion;
							var sNif			=	data.sDni;
							var sMatricula		=	data.sMatricula;
							sImporte		=   sImporte + parseInt(50);
							cont++;
							sCadena = sCadena+"<div class='alert alert-danger'><a class='close' data-dismiss='alert'>×</a> <strong>No vigente</strong> <a href='"+urlDisposicion+"' target='_blank'> Multa donde aparece el nif "+sNif+" con matricula "+sMatricula+"</div></a> <button id=\"recurrir\" name=\"recurrir\" class=\"btn btn-primary\"  >Recurrir la multa</button>";
							//sCadena = "<div class='resultadosContainer alert alert-danger'><a class='close' data-dismiss='alert'>×</a> <strong>No vigente</strong> <a href='"+urlDisposicion+"' target='_blank'> Tiene usted "+cont+" Multas donde aparece el nif "+sNif+" con matricula "+sMatricula+" con un importe de "+sImporte+"</div></a> <button id=\"recurrir\" name=\"recurrir\" class=\"resultadosContainer btn btn-primary\"  >Recurrir la multa</button>";
						});						
						
						if (cont>0)
							sCadena = "<div class='resultadosContainer alert alert-danger'><a class='close' data-dismiss='alert'>×</a> <strong>No vigente</strong>  Tiene usted "+cont+" multas con un importe de "+sImporte+"</div>"+sCadena;
						
						if (sCadena=="<div class=\"resultadosContainer\">")
							sCadena = sCadena+"<div  class=\"resultadosContainer alert alert-success\" ><a class=\"close\" data-dismiss=\"alert\">×</a>No existe ninguna multa para este Nif y Matricula</div>";
						sCadena = "<a href='https://play.google.com/store/apps/details?id=com.zerouno.ahorramultas' target='_blank' id='googleplay'><img src='./img/googleplay.gif' alt='app google play' style='height:12%;width:100%'/></a>"+ sCadena;
						sCadena = sCadena + "<div class=\"control-group\">"+
						  "<label class=\"control-label\" for=\"emailaviso\"></label>"+
						  "<div class=\"controls\">"+
						  "<br><div  class=\"resultadosContainer alert alert-success\" >No es obligatorio que nos des tu tel&eacute;fono. Pero as&iacute; podremos avisarte antes y recomendarte cual es la mejor forma de recurrir en tu caso.</div>"+
						    "<input id=\"emailaviso\" class=\"resultadosContainer form-control\" name=\"emailaviso\" placeholder=\"Email, de aviso\" class=\"input-xlarge\" required=\"\" type=\"text\">"+						   
						    "<input id=\"tlfnaviso\" class=\"resultadosContainer form-control\" name=\"tlfnaviso\" placeholder=\"Tel&eacute;fono, de aviso\" class=\"input-xlarge\" required=\"\" type=\"text\">"+
						  "</div>"+
						"</div><button id=\"busca\" name=\"busca\" class=\"resultadosContainer btn btn-primary\" onclick=\"registrarCliente();\" >Enviar</button>";
						$("."+sResultados+"multas").append(sCadena+"</div>");
					}); 
			
		}
	
		//oculta el carrusel apra mostrar la relación de multas
		function ocultaCarrusel(){
			if (($('#NIF').val()!=null) && ($('#matricula').val()!=null) && !(fCalcularNIF($('#NIF').val())||fCalcularNIE($('#NIF').val())||fCalcularCIF($('#NIF').val())))
				alert("error");
			else{
				$('#myCarousel').hide();
				var sResultados = "";
				if (window.outerWidth <= 980){						
					document.getElementById('resultadosMobile').style.visibility = 'visible';
					sResultados = "resultadosMobile";
				}
				else{
					document.getElementById('resultados').style.visibility = 'visible';
					sResultados = "resultados";
				}
					
				cargaMultas($('#NIF').val(),$('#matricula').val(),sResultados);
			}
		}
	
	</script>


	<jsp:include page="./includes/header.jsp" />

	<!-- /.navbar -->
	
	<div class="container" style="margin-top: 10px;">
		<%
			String sLista = com.locusiuris.utils.StringKeys.LISTASERVICIOS;
			String sLista2 = com.locusiuris.utils.StringKeys.LISTAENTRADAS;
			String sParametro = com.locusiuris.utils.StringKeys.DESDEINDEX;
			
			if ((Object) request.getSession().getAttribute(sLista) == null)
				response.sendRedirect("../../../servicioservlet?operacion="+ sLista + "&sitioweb=1&" + sParametro + "=1"); 
		%>

		<div class="row" style="margin-top: 60px;">
			<div class="col-xs-12 col-md-6 col-lg-4">
				<div class="well sidebar-nav">
				<!-- 	<form class="form-horizontal"> -->
						<fieldset>
							<!-- Form Name -->
							<legend>¿Tienes multas pendientes?</legend>
							<p class="help-block">Para averiguarlo usa este buscador</p>
							<!-- Text input-->
							<div class="control-group">
								<label class="control-label" for="NIF"></label>
								<div class="controls">
									<input id="NIF" name="NIF" placeholder="NIF/CIF/NIE"
										class="form-control"  type="text">
								</div>
							</div>

							<!-- Text input-->
							<div class="control-group">
								<label class="control-label" for="matricula"></label>
								<div class="controls">
									<input id="matricula" name="matricula" placeholder="Matricula"
										class="form-control"  type="text">
								</div>
							</div>

							<!-- Button -->
							<div class="control-group">
								<label class="control-label" for="busca"></label>
								<div class="controls">
									<button id="busca" name="busca" class="btn btn-primary" onclick="ocultaCarrusel()">Buscar
										Multas</button>
								</div>
							</div>

						</fieldset>
					<!-- </form> -->
				</div>
				<!--<jsp:include page="./includes/entradas.jsp" />-->
			</div>
			<div id="filaresultados" class="hidden-xs col-md-6 col-lg-8 ">
				<!-- <div class="visible-lg"> -->
				<div id="myCarousel" class="carousel slide "
					style="margin-top: -10px; margin-bottom: -10px;">
					<!-- Indicators -->
					<ol class="carousel-indicators">
						<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
						<li data-target="#myCarousel" data-slide-to="1"></li>
						<li data-target="#myCarousel" data-slide-to="2"></li>
					</ol>
					<div class="carousel-inner">
						<div class="item active">
							<img src="../../img/trailer.jpg">
							<div class="container">
								<div class="carousel-caption">
									
									<p style="font-size:18px;line-height:1;text-align:justify;">
										<b  >CONTRATA CON NOSOTROS A TRAVES DE LA APLICACION LA GESTION DE TODOS TUS EXPEDIENTES. SI PIERDES LOS PUNTOS EN EXPEDIENTES GESTIONADOS POR NUESTROS ABOGADOS TE INDEMNIZAMOS CON 1.500 EUROS MES HASTA QUE LOS RECUPERES.<br><br>ADEMAS PARA EL TRANSPORTISTA:
CONTRATA LA RECLAMACION DE DAÑOS POR PARALIZACIONES EN CONDICIONES SIN IGUAL EN EL MERCADO.

Y SOLICITA NUESTRAS OFERTAS EN PRODUCTOS PARA EL SECTOR SIN COMPETENCIA.</b>
									</p>
									<p>
										<!-- <a href="#" class="btn btn-info active"><i
											class="icon-white icon-eye-open"></i> Mas informaci&oacute;n</a> -->
									</p>
								</div>
							</div>
						</div>
						<div class="item">
							<!--  <img src="../../img/qrcode.jpg" data-src="holder.js/100%x500/auto/#777:#7a7a7a/text:First slide" alt="First slide"> -->
							<img src="../../img/pantallaandroid.png" alt="First slide">
							<div class="container">
								<div class="carousel-caption">
									<h1>App mobile</h1>

									<img src="../../img/qrcode.jpg">
									<p>
										<a class="btn btn-large btn-primary"
											target='_blank'
											href="https://play.google.com/store/apps/details?id=com.zerouno.ahorramultas">Descarga
											la aplicaci&oacute;n</a>
									</p>
								</div>
							</div>
						</div>
						<div class="item">
							<img src="../../img/redes.png">
							<div class="container">
								<div class="carousel-caption">

									<p>Siguenos en las redes sociales</p>

									<div class="fb-like" layout="button_count"
										data-href="https://www.facebook.com/pages/Alvarezabogadosdespacho/526673600750646"
										data-width="100" data-show-faces="false" data-send="false" style="margin-top: 16px;"></div>
									<br> <a href="https://twitter.com/alvarezdespacho"
										class="twitter-follow-button" data-show-count="false"
										data-lang="es" data-size="large" data-dnt="true">Seguir a
										@alvarezdespacho</a>
									<script>
										!function(d, s, id) {
											var js, fjs = d
													.getElementsByTagName(s)[0], p = /^http:/
													.test(d.location) ? 'http'
													: 'https';
											if (!d.getElementById(id)) {
												js = d.createElement(s);
												js.id = id;
												js.src = p
														+ '://platform.twitter.com/widgets.js';
												fjs.parentNode.insertBefore(js,
														fjs);
											}
										}(document, 'script', 'twitter-wjs');
									</script>

								</div>
							</div>
						</div>
					</div>
					<a class="left carousel-control" href="#myCarousel"
						data-slide="prev"><span
						class="glyphicon glyphicon-chevron-left"></span></a> <a
						class="right carousel-control" href="#myCarousel"
						data-slide="next"><span
						class="glyphicon glyphicon-chevron-right"></span></a>
				</div>
		
					<div id="resultados" class="jumbotron"
					style="margin-top: -120px; margin-bottom: -10px; visibility: hidden; ">
					<br>
                 	 <ul   class="resultadosmultas" >
				  		<br>
                    </ul>
					</div>
			</div>
			<div id="resultadosMobile" class="jumbotron"
					style="margin-top: -10px; margin-bottom: -10px; visibility: hidden; ">
				 <ul   class="resultadosMobilemultas" >
				  		<br>
                    </ul>
			</div>


			<!--/span-->

			<%
				/*  	 List<EntradaWebVO> oListEntradas = null;
						  if ((Object) request.getSession().getAttribute(
								com.locusiuris.utils.StringKeys.LISTASERVICIOS) != null) {
							oListEntradas = (List<EntradaWebVO>) request.getSession()
									.getAttribute(StringKeys.LISTAENTRADAS);

							int j = 0;
							String sListaEntradas = "<ul class=\"nav\" > <li><b>Blog</b></li>";

							while (j < 9 && j < oListEntradas.size()) {
								EntradaWebVO entrada = oListEntradas.get(j);

								sListaEntradas = sListaEntradas
										+ "<li ><a href=\"blog.jsp\">"
										+ entrada.getsTitulo() + "</a></li>";
								j++;
							}
							sListaEntradas = sListaEntradas + "</ul>";

							String sMenu = "<div class=\"col-xs-6 col-sm-3 sidebar-offcanvas\"  role=\"navigation\">"
									+ "<div class=\"well sidebar-nav\" > "
									+ sListaEntradas
									+ "	</div>" + "</div>";

							out.println(sMenu);
						}*/
			%>

			<!-- 	<div class="col-xs-6 col-sm-3 sidebar-offcanvas"  role="navigation">
           <div class="well sidebar-nav" > 
            <ul class="nav" >
              <li><b>Siguenos en las redes sociales</b></li>
               <li></li>
              <li ><div class="fb-like" data-href="https://www.facebook.com/pages/Alvarezabogadosdespacho/526673600750646" data-width="150"  data-show-faces="false" data-send="false" ></div></li>
              
              <li>	<a href="https://twitter.com/alvarezdespacho" class="twitter-follow-button" data-show-count="false" data-lang="es" data-size="large" data-dnt="true">Seguir a @alvarezdespacho</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script></li>
            </ul>
          </div>
        </div>  -->

		</div>
	
		<hr>
		<%
			List<ServicioWebVO> oListServicios = null;
			if ((Object) request.getSession().getAttribute(
					com.locusiuris.utils.StringKeys.LISTASERVICIOS) != null) {
				oListServicios = (List<ServicioWebVO>) request.getSession()
						.getAttribute(StringKeys.LISTASERVICIOS);
				int i = 0;
				String sHtml = "";
				if (oListServicios.size()>6)
				oListServicios = oListServicios.subList(0, 6);

				String sEntrada = "<div class=\"row\">"
						+ "<div class=\"col-6 col-sm-6 col-lg-4\" style=\"border-top: 2px dotted\">"
						+ "<h3>Titulo</h3>"
						+ "<p>Contenido</p>"
						+ "<p><a class=\"btn btn-default\" href=\"../../../servicioservlet?operacion="+StringKeys.DETALLESERVICIO+"&key=CLAVESERVICIO\" style=\"color:#ff9999;\" style=\"color:#ff9999;\" ><b>Ver Detalles &raquo;</b></a></p>"
						+ "</div>";

				for (ServicioWebVO servicio : oListServicios) {
					sEntrada = sEntrada
							.replace("Titulo", servicio.getsTitulo());
					sEntrada = sEntrada.replace("Contenido",
							"" + servicio.getsContenido());
					sEntrada = sEntrada.replace("CLAVESERVICIO",
							"" + servicio.getKey());
					sHtml = sHtml + sEntrada;
					sEntrada = ""
							+ "<div class=\"col-6 col-sm-6 col-lg-4\" style=\"border-top: 2px dotted\">"
							+ "<h3>Titulo</h3>"
							+ "<p>Contenido </p>"
							+ "<p><a class=\"btn btn-default\" href=\"../../../servicioservlet?operacion="+StringKeys.DETALLESERVICIO+"&key=CLAVESERVICIO\" style=\"color:#ff9999;\" style=\"color:#ff9999;\"><b>Ver Detalles &raquo;</b></a></p>"
							+ "</div>";
					i++;
				}
				sHtml = sHtml + "</div>";
				out.println(sHtml);	
			}
		%> 

		<jsp:include page="./includes/footer.jsp" />

	</div>
	<!--/.container-->


	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="../../assets/js/jquery.js"></script>
	<script src="../../dist/js/bootstrap.min.js"></script>
	<script src="offcanvas.js"></script>
	<script>
		(function(i, s, o, g, r, a, m) {
			i['GoogleAnalyticsObject'] = r;
			i[r] = i[r] || function() {
				(i[r].q = i[r].q || []).push(arguments)
			}, i[r].l = 1 * new Date();
			a = s.createElement(o), m = s.getElementsByTagName(o)[0];
			a.async = 1;
			a.src = g;
			m.parentNode.insertBefore(a, m)
		})(window, document, 'script',
				'//www.google-analytics.com/analytics.js', 'ga');

		ga('create', 'UA-44892985-1', 'alvarezdespacho.appspot.com');
		ga('send', 'pageview');
	</script>
</body>
</html>