<div id="myCarousel" class="carousel slide" data-ride="carousel"
					style="margin-top: 3px; margin-bottom: -10px;">
					<!-- Indicators -->
					<ol class="carousel-indicators">
						<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
						<li data-target="#myCarousel" data-slide-to="1"></li>
						<li data-target="#myCarousel" data-slide-to="2"></li>
					</ol>
					<div class="carousel-inner">
						<div class="item active">
							<img src="./img/trailer.jpg">
							<div class="container">
								<div class="carousel-caption">
									
									<!-- <p style="font-size:28px;line-height:1;text-align:justify;"> -->
									<h1 style="color:dd4814;"><b><strong  >INDEMNIZAMOS CON 1.500 EUROS AL MES</strong>, HASTA QUE RECUPERES LOS PUNTOS</b></h1>
									<!-- 	<b  >CONTRATA CON NOSOTROS A TRAVES DE LA APLICACION LA GESTION DE TODOS TUS EXPEDIENTES. SI PIERDES LOS PUNTOS EN EXPEDIENTES GESTIONADOS POR NUESTROS ABOGADOS TE INDEMNIZAMOS.<br><br>ADEMAS PARA EL TRANSPORTISTA:
CONTRATA LA RECLAMACION DE DA�OS POR PARALIZACIONES EN CONDICIONES SIN IGUAL EN EL MERCADO.

Y SOLICITA NUESTRAS OFERTAS EN PRODUCTOS PARA EL SECTOR SIN COMPETENCIA.</b> -->
									<!-- </p> -->
									<p>
										<a class="btn btn-large btn-primary"
											target='_blank'
											href="https://play.google.com/store/apps/details?id=com.zerouno.ahorramultas">Mas informaci&oacute;n</a>
									</p>
								</div>
							</div>
						</div>
						<div class="item">
							<!--  <img src="./web/img/qrcode.jpg" data-src="holder.js/100%x500/auto/#777:#7a7a7a/text:First slide" alt="First slide"> -->
							<img src="./img/pantallaandroid.png" alt="First slide">
							<div class="container">
								<div class="carousel-caption">
									<h1>App mobile</h1>

									<img src="./img/proximamente.png">
									<p>
										<a class="btn btn-large btn-primary"
											target='_blank'
											href="https://play.google.com/store/apps/details?id=com.zerouno.ahorramultas">Descarga
											la aplicaci&oacute;n</a>
									</p>
								</div>
							</div>
						</div>
						<div class="item">
							<img src="./img/redes2.png">
							<div class="container">
								<div class="carousel-caption">

									<p  style="font-size:28px;line-height:1;text-align:justify;">
										<b>SIGUENOS EN LAS REDES SOCIALES</b>
									</p>
									<div ><a href="https://twitter.com/alvarezdespacho"
									class="twitter-follow-button" data-show-count="false"
									data-lang="es" data-dnt="false"  >Seguir</a></div>
									
									<div style="margin-top: 3px;" class="fb-like-box" data-href="https://www.facebook.com/pages/Alvarezabogadosdespacho/526673600750646" data-colorscheme="light" data-show-faces="false" data-header="false" data-stream="false" data-show-border="false"></div>
				
									
									<script>
										!function(d, s, id) {
											var js, fjs = d
													.getElementsByTagName(s)[0], p = /^http:/
													.test(d.location) ? 'http'
													: 'https';
											if (!d.getElementById(id)) {
												js = d.createElement(s);
												js.id = id;
												js.src = p
														+ '://platform.twitter.com/widgets.js';
												fjs.parentNode.insertBefore(js,
														fjs);
											}
										}(document, 'script', 'twitter-wjs');
									</script>

								</div>
							</div>
						</div>
					</div>
					<a class="left carousel-control" href="#myCarousel"
						data-slide="prev"><span
						class="glyphicon glyphicon-chevron-left"></span></a> <a
						class="right carousel-control" href="#myCarousel"
						data-slide="next"><span
						class="glyphicon glyphicon-chevron-right"></span></a>
				</div>