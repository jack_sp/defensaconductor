<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@page import="com.locuiuris.vo.ServicioWebVO"%>
<%@page import="com.locusiuris.utils.StringKeys"%>
<html lang="en">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="../../assets/ico/favicon.png">

<title>BackOffice Alvarez Abogados</title>

<!-- Custom styles for this template -->
<link href="navbar.css" rel="stylesheet">





<!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
<!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->


<link rel="stylesheet" type="text/css"
	href="../../bootstrap2/lib/css/bootstrap.min.css"></link>
<link rel="stylesheet" type="text/css"
	href="../../bootstrap2/lib/css/prettify.css"></link>
<link rel="stylesheet" type="text/css"
	href="../../dist/css/bootstrap-wysihtml5.css"></link>

<script type="text/javascript">
	function seleccionaServicio() {
		var servicio = document.getElementById("servicio");
		var servicioEntrada = document.getElementById("serviciorequest").value;
		var i = 0;
		while (i <= servicio.options.length
				&& servicioEntrada.indexOf(servicio.options[i].value) == -1) {
			i++;
		}
		servicio.options[i].selected = true;
	}
</script>

</head>

<body onload="seleccionaServicio();">


	<div class="container">

		<div class="navbar navbar-inverse navbar-fixed-top">
			<div class="navbar-inner">
				<div class="container">
					<button type="button" class="btn btn-navbar" data-toggle="collapse"
						data-target=".nav-collapse">
						<span class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<a class="brand" href="./index.jsp">Back Office</a>
					<div class="nav-collapse collapse">
						<ul class="nav">
							<li><a href="./blog.jsp">Blog</a></li>
							<li class="active"><a href="./servicios.jsp">Servicios</a></li>
							<li><a href="#">Carrusel</a></li>
							<li><a href="https://www.google.com/analytics/web/?et&authuser=0#home/a590466w986427p970264/" target="_blank">Audiencias</a></li>
							<li><a href="#">Ayuda</a></li>
						</ul>
					</div>
					<!--/.nav-collapse -->
				</div>
			</div>
		</div>
<br>
		<br>
		<!-- Main component for a primary marketing message or call to action -->
		<div class="jumbotron">





			<form method="post"
				action="../../../servicioservlet?key=<%out.println(request.getParameter("key"));%>"
				id="form1">


				<input type='hidden' id="operacion" name="operacion" value="<%=StringKeys.EDITARSERVICIO%>">

				<div class="row-fluid">
					<div class="span4">
						<legend>Titulo</legend>
						<input type='text' id="titulo" name="titulo"
							value="<%out.println(((ServicioWebVO) request.getSession().getAttribute(
					StringKeys.SERVICIOVO)).getsTitulo());%>">
					</div>
					<div class="span4">
						<legend>Resum&eacute;n</legend>
						<textarea id="resumen" name="resumen" 
							style="width: 250px; height: 90px" maxlength="120"  cols=36 rows=4 ><%
							out.println(((ServicioWebVO) request.getSession().getAttribute(
									StringKeys.SERVICIOVO)).getsResumen());
						%></textarea>
					</div>
					<div class="span4">
						<legend>Categor&iacute;a</legend>
						<select id="servicio" name="servicio">
							<option value="volvo">Volvo</option>
							<option value="saab">Saab</option>
							<option value="mercedes">Mercedes</option>
							<option value="audi">Audi</option>
						</select>
					</div>
				</div>
				<div class="row-fluid">
					<div class="span12">
						<legend>Contenido</legend>
						
						<textarea id="notatextarea" name="notatextarea" class="textarea"
							style="width: 810px; height: 200px" cols=110 rows=6>
							<%
								out.println(((ServicioWebVO) request.getSession().getAttribute(
															StringKeys.SERVICIOVO)).getsContenido());
							%>
						</textarea>
					</div>
				</div>

				<input type="hidden" id="serviciorequest"
					value="<%out.println(((ServicioWebVO) request.getSession().getAttribute(
					StringKeys.SERVICIOVO)).getsServicio());%>">


				<input type='submit' class="btn btn-lg btn-primary">

			</form>


		</div>

	</div>
	<!-- /container -->

	<script src="../../bootstrap2/lib/js/wysihtml5-0.3.0.js"></script>
	<script src="../../bootstrap2/lib/js/jquery-1.7.2.min.js"></script>
	<script src="../../bootstrap2/lib/js/prettify.js"></script>
	<script src="../../bootstrap2/lib/js/bootstrap.min.js"></script>
	<script src="../../dist/js/bootstrap-wysihtml5.js"></script>

	<script>
		$('.textarea').wysihtml5();
	</script>

	<script type="text/javascript" charset="utf-8">
		$(prettyPrint);
	</script>

</body>
</html>