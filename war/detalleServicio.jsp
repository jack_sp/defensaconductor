<%@page import="com.locuiuris.vo.ServicioWebVO"%>
<%@page import="com.locusiuris.utils.StringKeys"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="es">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="DC.Title" content="Abogados Oviedo" />
<meta http-equiv="title" content="Abogados Oviedo" />
<meta http-equiv="description" content="Despacho de abogados en Oviedo" />
<meta http-equiv="DC.Description"
	content="Despacho de abogados en Oviedo" />
<meta name="keywords"
	content="abogados, oviedo, Villaviciosa de Asturias, abogados laboralistas, multas, tráfico, trafico" />
<meta name="REVISIT" content="2 days" />
<meta name="REVISIT-AFTER" content="2 days" />
<meta name="audience" content="Alle" />
<meta name="classification" content="web" />
<meta name="rating" content="general" />
<meta name="Robots" content="Index, Follow" />
<meta name="language" content="ES" />
<meta name="DC.Language" scheme="RFC1766" content="Spanish" />
<meta name="VW96.objecttype" content="Document" />
<meta name="distribution" content="global" />
<meta name="resource-type" content="document" />
<meta http-equiv="Last-Modified" content="0" />
<meta http-equiv="Cache-Control" content="no-cache, must-revalidate" />
<meta http-equiv="Content-Language" content="es" />
<meta name="author" content="Alberto Marqués">
<link rel="shortcut icon" href="../../assets/ico/favicon.png">

<title>DEFENSA DEL CONDUCTOR</title>

<!-- Bootstrap core CSS -->

<link href="./dist/css/bootstrap.css" rel="stylesheet">

<link href="./dist/css/bootstrap-theme.css" rel="stylesheet">
<link href="./css/locusweb.css" rel="stylesheet">
<link href="./css/carousel.css" rel="stylesheet">
<style type="text/css">
html {
	height: 100%
}

body {
	height: 100%;
	margin: 0;
	padding: 0
}
</style>
<!-- Custom styles for this template -->
<link href="./css/offcanvas.css" rel="stylesheet">
<script src="../../assets/js/html5shiv.js"></script>
<script src="../../assets/js/respond.min.js"></script>
<script src="js/jquery.js"></script>
<script type="text/javascript"
	src="http://maps.googleapis.com/maps/api/js?sensor=false">
	
</script>
<script type="text/javascript" src="js/home.js"></script>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->

</head>
<body>
	<jsp:include page="./includes/header.jsp" />

	<div class="container">

		<div class="row row-offcanvas row-offcanvas-right">
			<div class="col-xs-12 col-sm-9">

				<br> <br>

				<div class="row">
					<div class="col-xs-6 col-sm-4 col-lg-4">
						<br> <a
							href="../../../servicioservlet?operacion=<%=com.locusiuris.utils.StringKeys.LISTASERVICIOS%>&sitioweb=1">&laquo;<i>Ver
								todos los servicios</i></a>
					</div>
					<div class="col-xs-12 col-sm-8 col-lg-8 textocapitulo">
						<h2><%=((ServicioWebVO) request.getSession().getAttribute(
					StringKeys.SERVICIOVO)).getsTitulo()%></h2>
					</div>

				</div>


				<div class="row">
					<div class="col-xs-12 col-sm-8 col-lg-8">
						<%=((ServicioWebVO) request.getSession().getAttribute(
					StringKeys.SERVICIOVO)).getsContenido()%>
					</div>
					<div class="col-xs-6 col-sm-4 col-lg-4 ">
						<div class="imgcapitulo">
							<img src="../../img/derecho.jpg" style="width: 325px;">
						</div>
					</div>
				</div>


			</div>
			<!--/span-->
			<br> <br>

		</div>
		<!--/row-->

		<hr>

		<jsp:include page="./includes/footer.jsp" />

	</div>
	<!--/.container-->


	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="../../assets/js/jquery.js"></script>
	<script src="../../dist/js/bootstrap.min.js"></script>
	<script src="offcanvas.js"></script>
	<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-63787202-1', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>