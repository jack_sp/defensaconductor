<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html lang="es">
<head>
  <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="DC.Title" content="Abogados Oviedo" />
	<meta http-equiv="title" content="Abogados Oviedo" />
	<meta http-equiv="description" content="Despacho de abogados en Oviedo" />
	<meta http-equiv="DC.Description" content="Despacho de abogados en Oviedo" />
	<meta name="keywords" content="abogados, oviedo, Villaviciosa de Asturias, abogados laboralistas, multas, tráfico, trafico" />
	<meta name="REVISIT" content="2 days" />
	<meta name="REVISIT-AFTER" content="2 days" />
	<meta name="audience" content="Alle" />
	<meta name="classification" content="web" />
	<meta name="rating" content="general"/> 
	<meta name="Robots" content="Index, Follow" />
	<meta name="language" content="ES" />
	<meta name="DC.Language" scheme="RFC1766" content="Spanish" />
	<meta name="VW96.objecttype" content="Document" />
	<meta name="distribution" content="global" />
	<meta name="resource-type" content="document" />
	<meta http-equiv="Last-Modified" content="0" />
	<meta http-equiv="Cache-Control" content="no-cache, must-revalidate" />
	<meta http-equiv="Content-Language" content="es" />
	<meta name="author" content="Alberto Marqués">
<link rel="shortcut icon" href="../../assets/ico/favicon.png">

<title>ALVAREZ ABOGADOS</title>

<!-- Bootstrap core CSS -->

<link href="./dist/css/bootstrap.css" rel="stylesheet">

<link href="./dist/css/bootstrap-theme.css" rel="stylesheet">
<link href="./css/locusweb.css" rel="stylesheet">
<link href="./css/carousel.css" rel="stylesheet">
<style type="text/css">
html {
	height: 100%
}

body {
	height: 100%;
	margin: 0;
	padding: 0
}
h2 {color: #ffcc33;}
      h3 {color: #666666;}
</style>
<!-- Custom styles for this template -->
<link href="./css/offcanvas.css" rel="stylesheet">
<!-- <script src="../../assets/js/html5shiv.js"></script>
<script src="../../assets/js/respond.min.js"></script> -->
<script src="js/jquery.js"></script>
<script type="text/javascript"
	src="http://maps.googleapis.com/maps/api/js?sensor=false">
	
</script>
<script type="text/javascript" src="js/home.js"></script>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->

</head>

<body>
	<div id="fb-root"></div>
	<script>
		(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id))
				return;
			js = d.createElement(s);
			js.id = id;
			js.src = "//connect.facebook.net/es_LA/all.js#xfbml=1";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>
		<div class="navbar navbar-fixed-top" role="navigation">
		<div class="navbar-inner">
			<div class="container alturamenu">
				<div class="row alturamenu1">
					<div class=".col-xs-6 .col-lg-4"></div>
					<div class=".col-xs-12 .col-lg-8 ">
						<div class="sf-menu collapse navbar-collapse ">
							<br>
							<ul class="nav navbar-nav ">
								<li ><a href="../../../welcome">INICIO</a></li>
								<li><a href="quien.jsp">QUIENES SOMOS</a></li>
								<li><a
									href="../../../servicioservlet?operacion=<%=com.locusiuris.utils.StringKeys.LISTASERVICIOS%>&sitioweb=1">SERVICIOS</a></li>
								<li><a href="consulta.jsp">CONSULTAS</a></li>
								<li><a
									href="../../../blogservlet?operacion=<%=com.locusiuris.utils.StringKeys.LISTAENTRADAS%>&sitioweb=1">BLOG</a></li>
								<li class="active"><a href="donde.jsp">DONDE ESTAMOS</a></li>
								<li>
								<div class="fb-like" layout="button_count"
										data-href="https://www.facebook.com/pages/Alvarezabogadosdespacho/526673600750646"
										data-width="100" data-show-faces="false" data-send="false" style="margin-top: 16px;"></div>&nbsp;<a href="https://twitter.com/alvarezdespacho"
									class="twitter-follow-button" data-show-count="true"
									data-lang="es" data-dnt="false"  >Seguir</a></li>
								<li><br>
								<div style="margin-top: 1px;">
								</div></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-md-4 col-lg-3">
						<div class="navbar-header">
						<a class="navbar-brand" href="index.jsp"><img src="../../img/logo.png" style="height: 50px;"/></a>
							<button type="button" class="btn-custom navbar-toggle"
								data-toggle="collapse" data-target=".navbar-collapse">
								<span>Menu</span>
							</button>
							
						</div>
					</div>
					<div class="hidden-xs col-md-4 col-lg-5 "><br><div class="tel"><p><img alt="" src="./img/icon_mail.png" class="iconocontacto" >&nbsp;abogadosagb@gmail.com</p></div> </div>
					<div class="hidden-xs col-md-4 col-lg-4 "><br><div class="tel"><p><img alt="" src="./img/telefono.png" class="iconocontacto" >&nbsp;984.127.087</p></div> </div>
				</div>
			</div>
		</div>
	</div>
	<!-- /.navbar -->
	<br><br>
	<div class="container">

		<div class="row row-offcanvas row-offcanvas-right">
			<div class="col-xs-12 col-sm-9">



				<div class="row">
					<div class="col-xs-12 col-sm-8 col-lg-8"></div>
					<div class="col-xs-6 col-sm-4 col-lg-4 textocapitulo">
						<h2>Donde estamos</h2>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-6 col-sm-4 col-lg-4 ">
						<address class="datos_oficina linea">
							<p class="negrita">
								<strong>OVIEDO</strong>
							</p>
							<p>
								C/ Altamirano 3,<br>33003, Oviedo <br>España
							</p>

							<p>
								Tel&eacute;fono: 984.127.087<br>
							</p>
							<p>
								<a href="mailto:abogadosagb@gmail.com">abogadosagb@gmail.com</a>
							</p>

						</address>
					</div>
					<div class="col-xs-12 col-sm-8 col-lg-8">
						<div id="map-canvas" class="mapas"
							style="width: 600px; height: 300px;"></div>
					</div>

				</div>


			</div>
			<!--/span-->
			<br> <br>

		</div>
		<!--/row-->

		<hr>

			<footer>

			<div class="row">
				<div class="col-6 col-sm-6 col-lg-4">
					<h4>Contacto</h4>
					<p>
						Tel&eacute;fono: 984.127.087<br>
					</p>
					<p>despacho@locusiuris.com</p>
					<h4>Localizaci&oacute;n</h4>
					<p>
						C/ Altamirano 3,<br>33003, Oviedo <br>España
					</p>
				</div>
				<!--/span-->
				<div class="col-6 col-sm-6 col-lg-4">
				<br><br>
				
				</div>
				<!--/span-->
				<div class="col-6 col-sm-6 col-lg-4">
					
					<div class="social-links">
					<h4>Síguenos</h4>
					<ul>
					<li class="twitter"><a href="https://twitter.com/alvarezdespacho" title="Twitter" target="_blank">Twitter</a></li>
					<li class="facebook"><a href="https://www.facebook.com/pages/Alvarezabogadosdespacho/526673600750646" title="facebook" target="_blank">Facebook</a>
					</li>
					<li class="linkedin"><a href="http://www.linkedin.com/" title="LinkedIn" target="_blank">LinkedIn</a></li>
					
					</div>
					<p>
						
						<script>
							!function(d, s, id) {
								var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/
										.test(d.location) ? 'http' : 'https';
								if (!d.getElementById(id)) {
									js = d.createElement(s);
									js.id = id;
									js.src = p
											+ '://platform.twitter.com/widgets.js';
									fjs.parentNode.insertBefore(js, fjs);
								}
							}(document, 'script', 'twitter-wjs');
						</script>
					</p>
					<h4>Localizaci&oacute;n</h4>
					<p>
						Avda. Principado 20, 6&ordm; J,<br>33404. Corvera de Asturias <br>España
					</p>
					
					<p>
						Plaza del Ayuntamiento 20, 1&ordm; izda.,<br>33300. Villaviciosa de Asturias<br>España
					</p>
				</div>
				<!--/span-->
			</div>
			<!--/row-->
			<hr>
			<p>&copy; Alvarez Abogado 2013</p>
		</footer>

	</div>
	<!--/.container-->


	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="../../assets/js/jquery.js"></script>
	<script src="../../dist/js/bootstrap.min.js"></script>
	<script src="offcanvas.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-44892985-1', 'alvarezdespacho.appspot.com');
  ga('send', 'pageview');

</script>
</body>
</html>
