<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html lang="es">
  <head>
      <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="DC.Title" content="Abogados Oviedo" />
	<meta http-equiv="title" content="Abogados Oviedo" />
	<meta http-equiv="description" content="Despacho de abogados en Oviedo" />
	<meta http-equiv="DC.Description" content="Despacho de abogados en Oviedo" />
	<meta name="keywords" content="abogados, oviedo, Villaviciosa de Asturias, abogados laboralistas, multas, tráfico, trafico" />
	<meta name="REVISIT" content="2 days" />
	<meta name="REVISIT-AFTER" content="2 days" />
	<meta name="audience" content="Alle" />
	<meta name="classification" content="web" />
	<meta name="rating" content="general"/>
	<meta name="Robots" content="Index, Follow" />
	<meta name="language" content="ES" />
	<meta name="DC.Language" scheme="RFC1766" content="Spanish" />
	<meta name="VW96.objecttype" content="Document" />
	<meta name="distribution" content="global" />
	<meta name="resource-type" content="document" />
	<meta http-equiv="Last-Modified" content="0" />
	<meta http-equiv="Cache-Control" content="no-cache, must-revalidate" />
	<meta http-equiv="Content-Language" content="es" />
	<meta name="author" content="Alberto Marqués">
    <link rel="shortcut icon" href="../../assets/ico/favicon.png">

    <title>DEFENSA DEL CONDUCTOR</title>

    <!-- Bootstrap core CSS -->

<link href="../../dist/css/bootstrap.css" rel="stylesheet">

<link href="../../dist/css/bootstrap-theme.css" rel="stylesheet">
    <link href="./css/locusweb.css" rel="stylesheet">
	<link href="./css/carousel.css" rel="stylesheet">
	<style type="text/css">
      html { height: 100% }
      body { height: 100%; margin: 0; padding: 0 }
    h2 {color: #ffcc33;}
      h3 {color: #ffcc33;}


    </style>
    <!-- Custom styles for this template -->
    <link href="./css/offcanvas.css" rel="stylesheet">
    <script src="../../assets/js/html5shiv.js"></script>
    <script src="../../assets/js/respond.min.js"></script>
    <script src="js/jquery.js"></script>
   <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false">
    </script>
	<script type="text/javascript" src="js/home.js"></script>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
    <script>
	function validacion() {
		var camposrequeridos ="";
    var valido = true;
    $("#errorFormulario").hide();
    $("#errorFormulario ul").html("");

    if ($('#nombre').val()==''){
      $("#errorFormulario ul").append("<li>El campo nombre est&aacute; vacio</li>");
    }

    if ($('#telefono').val()!=""){
      if(!validaTelefono($('#telefono').val()))
        $("#errorFormulario ul").append("<li>El campo tel&eacute;fono es incorrecto</li>");
    }

    if ($('textarea#consulta').val()==''){
      $("#errorFormulario ul").append("<li>El campo consulta est&aacute; vacio</li>");
    }

    if ($('#email').val()==''){
      $("#errorFormulario ul").append("<li>El campo email est&aacute; vacio</li>");
    } else {
      valido = validarEmail($('#email').val());
    }

	  if ($("#errorFormulario ul").html()!="") {
	    $("#errorFormulario").show();
	  }

	  return $("#errorFormulario ul").html()=="";
	}

	function confirmacionCorreo() {
		$( "#consulta" ).append( "<p>Correo enviado con exito</p>" );
	}

  function validarEmail(valor) {
    if (/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/.test(valor)){
      // alert("La dirección de email " + valor + " es correcta.");
      return true;
    } else {
      $("#errorFormulario ul").append("<li>La dirección de email " + valor + " es incorrecta.</li>");
      return false;
    }
  }

  function validaTelefono(telefono){
    var RegExPatternX = new RegExp("[0123456789 -]");
    var errorMessage = 'El numero de telefono solo puede contener numeros, espacios y "-"';

    if(telefono != ""){

      if (telefono.match(RegExPatternX)) {
        return true;
      }
      else {
        $(".alert-danger").html(errorMessage);
        return false;
      }
    }
  }


    </script>

  </head>

  <body  >
    <div id="fb-root"></div>
	<script src="./js/facebook.js"></script>
	<jsp:include page="./includes/header.jsp" />
    <div class="container" style="margin-top: 35px;">

       <div class="row">
       	<div class="col-xs-6 col-sm-4 col-lg-4 " >
       	<address class="datos_oficina linea"  style=" font-family: Georgia, 'Times New Roman', serif;">
        <h3> Alvarez Abogados</h3>
            <p>C/ Altamirano 3,<br>33003, Oviedo <br>España</p>

            <p>Tel&eacute;fono: 984.04.12.75<br>

                     </p>
                     <p><a href="mailto:info@defensadelconductor.com">info@defensadelconductor.com</a></p>

        </address>
         </div>


        <div class="col-xs-12 col-sm-8 col-lg-8" id="consulta" name="consulta">
            <form class="form-horizontal" action="../../../mailservlet" method="post"  onsubmit="return validacion()" >
            <fieldset>

            <!-- Form Name -->
            <h2>Consulta</h2>

            <div id="errorFormulario" class="alert alert-danger" style="display:none;" ><a class=\"close\" data-dismiss=\"alert\">×</a><strong>Error!</strong><ul></ul></div>

            <%
	            if (request.getParameter("mail")!=null){
	            	if (request.getParameter("mail").equals("ok"))
	             		out.println("<div  class=\"alert alert-success\" ><a class=\"close\" data-dismiss=\"alert\">×</a>Consulta enviada con exito</div>");
	            	else if (request.getParameter("mail").equals("error"))
	            		out.println("<div class=\"alert alert-danger\">"+
	            		"<a class=\"close\" data-dismiss=\"alert\">×</a> <strong>Error!</strong>No ha podido enviarse la consulta, intentelo más tarde<br>"+
	            		"o dirijase al correo info@defensadelconductor.com</div>");
	            }
            %>
            <!-- Text input-->
            <div class="control-group">
              <label class="control-label"></label>
              <div class="controls">
                <input id="nombre" name="nombre" class="form-control" size="35" type="text" placeholder="Nombre *" class="input-xlarge" >

              </div>
            </div>

            <!-- Text input-->
            <div class="control-group">
              <label class="control-label"></label>
              <div class="controls">
      <input id="email" name="email" size="35" class="form-control" type="text" placeholder="email *" class="input-xlarge"  >

              </div>
            </div>

            <!-- Text input-->
            <div class="control-group">
              <label class="control-label"></label>
              <div class="controls">
                <input id="telefono" name="telefono" class="form-control" type="text" placeholder="Tel&eacute;fono" class="input-xlarge" onSubmit="validaTelefono(document.getElementById('telefono').value)">

              </div>
            </div>

            <!-- Text input-->
            <div class="control-group">
              <label class="control-label"></label>
              <div class="controls">
                <input id="ciudad" name="ciudad" class="form-control" type="text"  class="input-xlarge" placeholder="Ciudad">

              </div>
            </div>

            <!-- Textarea -->
            <div class="control-group">
              <label class="control-label"></label>
              <div class="controls">
                <textarea id="consulta" name="consulta" class="form-control" placeholder="Consulta *" rows="4" cols="80" ></textarea>
              </div>
            </div>

            <br>
            <input type="submit" class="btn btn-info active" value="Enviar consulta"  />

            </fieldset>
            </form>
            </div>
          </div><!--/row-->

       <br>   <br>   <br>

      <hr>

     	<jsp:include page="./includes/footer.jsp" />

    </div><!--/.container-->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../../assets/js/jquery.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>
    <script src="offcanvas.js"></script>
 <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-63787202-1', 'auto');
  ga('send', 'pageview');

</script>
  </body>
</html>
