<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html lang="es">
<head>
  <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="DC.Title" content="Abogados Oviedo" />
	<meta http-equiv="title" content="Abogados Oviedo" />
	<meta http-equiv="description" content="Despacho de abogados en Oviedo" />
	<meta http-equiv="DC.Description" content="Despacho de abogados en Oviedo" />
	<meta name="keywords" content="abogados, oviedo, Villaviciosa de Asturias, abogados laboralistas, multas, tráfico, trafico" />
	<meta name="REVISIT" content="2 days" />
	<meta name="REVISIT-AFTER" content="2 days" />
	<meta name="audience" content="Alle" />
	<meta name="classification" content="web" />
	<meta name="rating" content="general"/> 
	<meta name="Robots" content="Index, Follow" />
	<meta name="language" content="ES" />
	<meta name="DC.Language" scheme="RFC1766" content="Spanish" />
	<meta name="VW96.objecttype" content="Document" />
	<meta name="distribution" content="global" />
	<meta name="resource-type" content="document" />
	<meta http-equiv="Last-Modified" content="0" />
	<meta http-equiv="Cache-Control" content="no-cache, must-revalidate" />
	<meta http-equiv="Content-Language" content="es" />
	<meta name="author" content="Alberto Marqués">
<link rel="shortcut icon" href="../../assets/ico/favicon.png">

<title>DEFENSA DEL CONDUCTOR</title>

<link href="../../dist/css/bootstrap.css" rel="stylesheet">
<link href="./css/locusweb.css" rel="stylesheet">
<link href="./css/carousel.css" rel="stylesheet">

<style type="text/css">
html {
	height: 100%
}

body {
	height: 100%;
	margin: 0;
	padding: 0;
}

h3 {
	color: #ffcc33;
}
h2 {
	color: #ffcc33;
}
</style>
<!-- Custom styles for this template -->
<link href="./css/offcanvas.css" rel="stylesheet">
<script src="../../assets/js/html5shiv.js"></script>
<script src="../../assets/js/respond.min.js"></script>
<script src="js/jquery.js"></script>
<script type="text/javascript"
	src="http://maps.googleapis.com/maps/api/js?sensor=false">
	
</script>
<script type="text/javascript" src="js/home.js"></script>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->

</head>

<body>
	<div id="fb-root"></div>
	<script src="./js/facebook.js"></script>
	
		<jsp:include page="./includes/header.jsp" />
	<!-- /.navbar -->

	<div class="container" style="margin-top: 35px;">

		<div class="row row-offcanvas row-offcanvas-right">
			<div class="col-xs-12 col-sm-9">

		

				<div class="row">
					<div class="col-xs-12 col-sm-8 col-lg-8">
						<h3>Recursos gratis</h3>
					</div>
					<div class="col-xs-6 col-sm-4 col-lg-4 ">
					</div>
				</div>


				<div class="row">
					<div class="col-xs-12 col-sm-8 col-lg-8">
						<br><p>
						En esta secci&oacute;n encontraras una selecci&oacute;n de recursos gratuitos, que podr&aacute;s utilizar para reclamar tu mismo tus multas.<br>
						 Para cualquier duda no dudes en consultarnos nosotros te atenderemos y orientaremos sobre cual es la mejor manera de actuar, dependiendo de tu caso. 
						</p><br>
		 				
						<div class="list-group">
						
						
						  <a href="#" class="list-group-item active">Recursos sobre tr&aacute;fico<a>
  
  <a href="./recursos/trafico/carece_autorizacion_carga_y_descarga.doc" class="list-group-item" target="_blank">Carece autorizaci&oacute;n carga y descarga</a>
  <a href="./recursos/trafico/circular_bajo_los_efectos_del_alcohol_o_drogas.doc" class="list-group-item" target="_blank">Circular bajo los efectos del alcohol o drogas</a>
  <a href="./recursos/trafico/circular_entorpeciendo_el_trafico.doc" class="list-group-item" target="_blank">Circular entorpeciendo el tráfico</a>
  <a href="./recursos/trafico/circular_sin_placas_de_matricula.doc" class="list-group-item" target="_blank">Circular sin placas de matricula</a>
  <a href="./recursos/trafico/CIRCULAR_SIN_RESPETAR_DISTANCIA_SEGURIDAD.doc" class="list-group-item" target="_blank">Circular sin respetar distancia seguridad</a>
  <a href="./recursos/trafico/circular_usando_el_telefono_movil.doc" class="list-group-item" target="_blank">Circular usando el teléfono móvil</a>
  <a href="./recursos/trafico/ESTACIONAR_carril_reservado.doc" class="list-group-item" target="_blank">Estacionar carril reservado</a>
  <a href="./recursos/trafico/Estacionar_en_carril_circulacion.doc" class="list-group-item" target="_blank">Estacionar en carril circulacion</a>
  <a href="./recursos/trafico/Estacionar_en_lugar_prohibido.doc" class="list-group-item" target="_blank">Estacionar en lugar prohibido</a>
  <a href="./recursos/trafico/Estacionar_sobre_la_acera.doc" class="list-group-item" target="_blank">Estacionar sobre la acera</a>
  <a href="./recursos/trafico/Neumaticos_en_mal_estado.doc" class="list-group-item" target="_blank">Neumaticos en mal estado</a>
  <a href="./recursos/trafico/No_identificar_conductor.doc" class="list-group-item" target="_blank">No identificar conductor</a>
  <a href="./recursos/trafico/no_senializar_con_antelacion_una_maniobra.doc" class="list-group-item" target="_blank">no señalizar con antelacion una maniobra</a>
  <a href="./recursos/trafico/SOLICITUD_FRACCIONAMIENTO_DEUDA.doc" class="list-group-item" target="_blank">Solicitud fraccionamiento deuda</a>
  
  
  
   
						  <a href="#" class="list-group-item active">Recursos transporte<a>

  <a href="./recursos/transporte/Carencia_de_hojas_de_registro.doc" class="list-group-item" target="_blank">Carencia de hojas de registro</a>
  <a href="./recursos/transporte/circular_ tramo_restringido_sin_autorizacion.doc" class="list-group-item" target="_blank">Circular tramo restringido sin autorizaci&oacute;n</a>
  <a href="./recursos/transporte/Discos_diagrama_no_insertados.doc" class="list-group-item" target="_blank">Discos diagrama no insertados</a>
  <a href="./recursos/transporte/Discos_diagrama_superposicion_y_minoracion_descanso.doc" class="list-group-item" target="_blank">Discos diagrama superposición y minoracion descanso</a>
  <a href="./recursos/transporte/exceso_de_peso.doc" class="list-group-item" target="_blank">Exceso de peso</a>
  <a href="./recursos/transporte/Falsear_Discos_diagrama.doc" class="list-group-item" target="_blank">Falsear Discos diagrama</a>
  <a href="./recursos/transporte/falta_de_autorizacion_para_EMISORA.doc" class="list-group-item" target="_blank">Falta de autorización para EMISORA</a>
  <a href="./recursos/transporte/falta_de_extintores.doc" class="list-group-item" target="_blank">Falta de extintores</a>
  <a href="./recursos/transporte/limitador_velocidad_inadecuado_funcionamiento.doc" class="list-group-item" target="_blank">Limitador velocidad inadecuado funcionamiento</a>
  <a href="./recursos/transporte/manipulacion_placa_montaje_limitador.doc" class="list-group-item" target="_blank">Manipulaci&oacute;n placa montaje limitador</a>
  <a href="./recursos/transporte/minoracion_del_descanso_semanal_obligatorio.doc" class="list-group-item" target="_blank">Minoracion del descanso semanal obligatorio</a>
  <a href="./recursos/transporte/minoracion_descanso_diari_hasta_20.doc" class="list-group-item" target="_blank">Minoraci&oacute;n descanso diario hasta 20%</a>
  <a href="./recursos/transporte/minoracion_descanso_diario_obligatorio.doc" class="list-group-item" target="_blank">Minoracion descanso diario obligatorio</a>
  <a href="./recursos/transporte/no_presentar_permiso_circulacion.doc" class="list-group-item" target="_blank">No presentar permiso circulaci&oacute;n</a>
  <a href="./recursos/transporte/reforma_de_importancia.doc" class="list-group-item" target="_blank">Reforma de importancia</a>
  <a href="./recursos/transporte/un_diisco_diagrama_utilizacion_durante_varias_jornadas.doc" class="list-group-item" target="_blank">Un disco diagrama utilizaci&oacute;n durante varias jornadas</a>
  						  

</div>
					</div>
					<div class="col-xs-6 col-sm-4 col-lg-4 ">
						<div class="imgcapitulo">
				
							<img src="../../img/gratis.jpg" style="width: 325px;">
						</div>
					</div>
				</div>


			</div>
			<!--/span-->
			<br> <br>

		</div>
		<!--/row-->

		<hr>

	<jsp:include page="./includes/footer.jsp" />

	</div>
	<!--/.container-->


	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="../../assets/js/jquery.js"></script>
	<script src="../../dist/js/bootstrap.min.js"></script>
	<script src="offcanvas.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-63787202-1', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>
