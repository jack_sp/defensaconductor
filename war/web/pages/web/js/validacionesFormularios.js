var arrayLetras = new Array(44);
// La posición 23 nunca sale, está en el array para las posiciones posteriores a la 23.
arrayLetras = new Array("T", "R", "W", "A", "G", "M", "Y", "F", "P", "D", "X", "B", "N", "J", "Z", "S", "Q",
                                   "V", "H", "L", "C", "K", "E", "T", "0", "9", "8", "7", "6", "5", "4", "3", "2", "1",
                                   "J", "I", "H", "G", "F", "E", "D", "C", "B", "A");
 
function fCalcularNIF(nif) {
    var correcto = true;
    var resto = 0;
    var dni;
    var letra;
 
    nif = nif.toUpperCase();
    if (nif.length < 9) // El DNI tiene que tener una logitud de 9 caracteres.
    {
        return false;
    }
 
    dni = nif.substring(0, nif.length - 1);
    // Comprueba que el dni está compuesto sólo por números
    for (i = 0; i < dni.length; i++) {
        ch = dni.charAt(i);
        if (isNaN(ch)) {
            return false;
        }
    }
 
    resto = dni % 23;
 
    if (arrayLetras[resto] == nif.charAt(nif.length - 1).toUpperCase()) {
        return true;
    }
    else {
        return false;
    }
}
 
 
// Funcion que comprueba que el parámetro pasado como valor es
// un NIE válido.
// nie: string que contiene el valor del NIE a validar
function fCalcularNIE(nie) {
 
    nie = nie.toUpperCase();
    //alert(nie);
    var letraX;
    var dni = 0;
 
    // Si el nie no tiene 3 caracteres o más salimos de la función
    if (nie.length < 9) {
        return false;
    }
 
    // Cogemos la primera posición del parametro
    letraX = nie.substring(0, 1);
    // Si la primera posicion del nie es una X o una K o una L o una M.
    if ((letraX = 'X') || (letraX = 'K') || (letraX = 'L') || (letraX = 'M')) {
        // Obtengo el número del nie.
        dni = nie.substring(1, nie.length - 1);
        // Comprueba que el dni está compuesto sólo por números
        for (i = 0; i < dni.length; i++) {
            ch = dni.charAt(i);
            if (isNaN(ch)) {
                return false;
            }
        }
    }
    else {
        return false;
    }
    resto = dni % 23;
    var letra = arrayLetras[resto];
    if (letra == nie.charAt(nie.length - 1).toUpperCase()) {
        return true;
    }
    else {
        return false;
    }
 
}
 
// Funcion que comprueba que el parámetro pasado como valor es
// un CIF válido.
// cif: string que contiene el valor del CIF a validar
function fCalcularCIF(cif) {
 
    cif = cif.toUpperCase();
    var acumulado = 0;
    var resto = 0;
    var valorFinal = 0;
    var codigoEntidad;
    var numero;
    var digitoControl;
 
    // Si el cif no tiene 3 caracteres o más salimos de la función
    if (cif.length < 9) {
        return false;
    }
    // Cogemos el primer caracter del CIF.
    codigoEntidad = cif.substring(0, 1);
 
    // Cogemos el número del cif que son todos los dígitos menos el primero y el último.
    numero = cif.substring(1, cif.length - 1);
 
    // Cogemos el último parámetro del CIF.
    digitoControl = cif.substring(cif.length - 1, cif.length);
 
    for (i = 0; i < numero.length; i++) {
        var ch = numero.charAt(i);
        if (isNaN(ch)) {
            // Si el caracter que estamos tratando no es un numero, no pasa la validación.
            return false;
        }
        if ((i + 1 == 1) || (i + 1 == 3) || (i + 1 == 5) || (i + 1 == 7)) {
            aux = ch * 2;
            if (aux > 9) {
                acumulado = acumulado + (aux % 10) + (aux / 10);
            }
            else {
                acumulado = parseInt(acumulado) + parseInt(aux);
            }
        }
        else {
            acumulado = parseInt(acumulado) + parseInt(ch);
        }
    }
 
    resto = parseInt(acumulado) % 10;
    if ((codigoEntidad == 'P') || (codigoEntidad == 'Q') || (codigoEntidad == 'S')) //|| (codigoEntidad=='Z') )
    {
        valor = resto + 34;
    }
    /*   
    else if ( (codigoEntidad=='A') && isNaN(digitoControl) )
    {
    if ( confirm('Está dando el alta de una empresa extranjera, ¿desea continuar?') )
    {
    valor = resto + 34;
    }
    else
    {
    return false;
    }
    }
    */
    else {
        valor = resto + 24;
    }
 
    var letra = arrayLetras[valor];
    if (letra == digitoControl) {
        return true;
    }
    else {
        return false;
    }
}
 
function fCalcularCIE(cie) {
    arrayLetrasCIF = new Array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J");
 
    cie = cie.toUpperCase();
    var acumulado = 0;
    var resto = 0;
    var valorFinal = 0;
    var complemento = 0;
    var codigoEntidad;
    var numero;
    var digitoControl;
 
    // Cogemos el primer caracter del CIE.
    codigoEntidad = cie.substring(0, 1);
 
    // Cogemos el número del cie que son todos los dígitos menos el primero y el último.
    numero = cie.substring(1, cie.length - 1);
 
    // Cogemos el último parámetro del CIE.
    digitoControl = cie.substring(cie.length - 1, cie.length);
 
    for (i = 0; i < numero.length; i++) {
        var ch = numero.charAt(i);
        if (isNaN(ch)) {
            // Si el caracter que estamos tratando no es un numero, no pasa la validación.
            return false;
        }
        if ((i + 1 == 1) || (i + 1 == 3) || (i + 1 == 5) || (i + 1 == 7)) {
            aux = ch * 2;
            if (aux > 9) {
                // acumulado = acumulado + la ultima cifra de aux + la primera cifra de aux
                acumulado = acumulado + (aux % 10) + (aux / 10);
            }
            else {
                acumulado = parseInt(acumulado) + parseInt(aux);
            }
        }
        else {
            acumulado = parseInt(acumulado) + parseInt(ch);
        }
    }
 
    resto = parseInt(acumulado) % 10;
    complemento = 10 - resto;
 
    var letra = arrayLetrasCIF[complemento - 1];
    if (letra == digitoControl) {
        return true;
    }
    else {
        return false;
    }
 
}
 
function fCalcularDNI(dni) {
    if (dni.length != 8) // El DNI tiene que tener una logitud de 8 caracteres.
    {
        return false;
    }
 
    // Comprueba que el dni está compuesto sólo por números
    for (i = 0; i < dni.length; i++) {
        ch = dni.charAt(i);
        if (isNaN(ch)) {
            return false;
        }
    }
 
    return true;
}
 
function fCalcularPasaporte(pasaporte) {
    var contadorDeLetras = 0;
    for (i = 0; i < pasaporte.length; i++) {
        ch = pasaporte.charAt(i);
        if (isNaN(ch)) {
            contadorDeLetras = contadorDeLetras + 1;
        }
    }
 
    if (contadorDeLetras == pasaporte.length) {
        return false;
    }
 
    return true;
}
 
function fCalcularTarjeta(tarjeta) {
    var contadorDeLetras = 0;
    for (i = 0; i < tarjeta.length; i++) {
        ch = tarjeta.charAt(i);
        if (isNaN(ch)) {
            contadorDeLetras == contadorDeLetras + 1;
        }
    }
 
    if (contadorDeLetras == tarjeta.length) {
        return false;
    }
 
    return true;
}
 
function validarXIF() {
 
 
    var _doc = Xrm.Page.getAttribute("asisa_numero").getValue();
    var tipoDoc = Xrm.Page.getAttribute("asisa_tipodedocumento").getValue();
 
    var Tipo = "";
 
    if (_doc.length > 0) {
 
        switch (tipoDoc) {
 
            case 100000000: Tipo = "DNI"; break;
            case 100000001: Tipo = "NIE"; break;
            case 100000002: Tipo = "NIF"; break;
            case 100000003: Tipo = "Pasaporte"; break;
            case 100000004: Tipo = "Tarjeta de residencia"; break;
            case 100000006: Tipo = "CIF"; break;
            default: break;
 
        }
 
 
        switch (Tipo) {
            case "CIF": valor = fCalcularCIF(_doc); break;
            case "DNI": valor = fCalcularNIF(_doc); break;
            case "NIE": valor = fCalcularNIE(_doc); break;
            case "NIF": valor = fCalcularNIF(_doc); break;
            case "Pasaporte": valor = fCalcularPasaporte(_doc); break;
            case "Tarjeta de residencia": valor = fCalcularTarjeta(_doc); break;
 
            default: valor = false; break;
 
        }
 
 
        if (!valor) {
            alert("No es un valor correcto para " + Tipo);
        }
 
 
    }
 
 
    function validacion() {
		var camposrequeridos ="";
		
	  if (document.getElementById('nombre')!='' &&
			  document.getElementById('telefono')!='' &&
			  document.getElementById('consulta')!='') {
		  $("#alert-success").show();
	  }else
		  return false;
	 
	 
	  return true;
	}
	
	function confirmacionCorreo() {
		$( "#consulta" ).append( "<p>Correo enviado con exito</p>" );
	}
    	function validarEmail(valor) {
				if (/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/.test(valor)){
		// alert("La dirección de email " + valor + " es correcta.");
		} else {
		 alert("La dirección de email " + valor + " es incorrecta.");
		}
		}
		function validaTelefono(telefono){
		var RegExPatternX = new RegExp("[0123456789 -]"); 
		var errorMessage = 'El numero de telefono solo puede contener numeros, espacios y "-"';
		
			if(telefono != ""){ 
			
			if (telefono.match(RegExPatternX)) { 
			} 
			else {
			
			alert(errorMessage); 
			} 
			}
		}
		
 
}