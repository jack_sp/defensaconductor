package com.locusiuris.db;

import java.util.Date;
import java.util.List;

import javax.jdo.PersistenceManager;

import com.google.appengine.api.datastore.Text;
import com.locuiuris.vo.ServicioWebVO;
import com.serverrest.jdo.PMF;

public class ServiciosDB {
	
	
	public void guardarServicio(ServicioWebVO oServicioVO) {
		 PersistenceManager pm = PMF.get().getPersistenceManager();
	        try {
	        	oServicioVO.setoDateFechaEntrada(new Date());
	            pm.makePersistent(oServicioVO);
	        } finally {
	            pm.close();
	        }
	}
	
	public List<ServicioWebVO> listadoServicio() {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		javax.jdo.Query query = pm.newQuery(ServicioWebVO.class);
		
        return(List<ServicioWebVO>) query.execute(query) ; 
	}

	public void borradoServicio(String id) {
		PersistenceManager pm = PMF.get().getPersistenceManager();
        try {
        	ServicioWebVO oServicioVO = pm.getObjectById(ServicioWebVO.class, (new Double(id)).longValue());
            pm.deletePersistent(oServicioVO);
        } finally {
            pm.close();
        }
	}

	public void actualizaServicio(String id,ServicioWebVO oServicioVO1) {
		 PersistenceManager pm = PMF.get().getPersistenceManager();
		    try {
		    	ServicioWebVO oServicioVO = pm.getObjectById(ServicioWebVO.class, (new Double(id)).longValue());
		    	oServicioVO.setsTitulo(oServicioVO1.getsTitulo());
		    	oServicioVO.setsContenido(new Text(oServicioVO1.getsContenido()));
		    	oServicioVO.setsServicio(oServicioVO1.getsServicio());
		    	oServicioVO.setsResumen(oServicioVO1.getsResumen());
		    	oServicioVO.setoDateFechaEntrada(new Date());
	        } finally {
	            pm.close();
	        }
	}
	
	public ServicioWebVO consultaServicio(String id) {
		 PersistenceManager pm = PMF.get().getPersistenceManager();
		 ServicioWebVO oServicioVO = null;
		    try {
		    	oServicioVO = pm.getObjectById(ServicioWebVO.class, (new Double(id)).longValue());
	        } finally {
	            pm.close();
	        }
		 return oServicioVO;
	}

}
