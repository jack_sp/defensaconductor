package com.locuiuris.vo;

import java.util.Date;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Text;

@PersistenceCapable(detachable = "true")
public class ServicioWebVO implements java.io.Serializable {

	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		@Persistent
		private Text sContenido;
		 public void setsContenido(Text sContenido) {
			this.sContenido = sContenido;
		}
		@Persistent
		 private String sTitulo;
		 @Persistent
		 private String sServicio;
		 @Persistent
		 private String sResumen;

		 public String getsResumen() {
			return sResumen;
		}
		public void setsResumen(String sResumen) {
			this.sResumen = sResumen;
		}
		public long getKey() {
			return key.getId();
		}
		public void setKey(Key key) {
			this.key = key;
		}
		@PrimaryKey
		    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
		    private Key key;
		 
		 
		@Persistent
		Date oDateFechaEntrada;
		 
		
		public String getsContenido() {
			return sContenido.getValue();
		}
		public String getsTitulo() {
			return sTitulo;
		}
		public Date getoDateFechaEntrada() {
			return oDateFechaEntrada;
		}
		public void setoDateFechaEntrada(Date oDateFechaEntrada) {
			this.oDateFechaEntrada = oDateFechaEntrada;
		}
		public void setsTitulo(String sTitulo) {
			this.sTitulo = sTitulo;
		}
		public String getsServicio() {
			return sServicio;
		}
		public void setsServicio(String sServicio) {
			this.sServicio = sServicio;
		}

	
}
