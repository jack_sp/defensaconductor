package com.locusiuris.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;

public class AcortaUrl {

    private String path;
    private String user;
    private String key;

    public AcortaUrl() {
        this.path = "http://api.bit.ly/v3/";
        this.user = StringKeys.USERBIT;
        this.key = StringKeys.PASSWORD;
    }

    public String Shorten(String url) throws IOException {
        String data = "";
      
       // String temp = this.path + "shorten?login=" + this.user + "&apiKey=" + this.key + "&uri=" + url + "&format=txt";
        String temp = this.path + "shorten?login=" + this.user + "&apiKey=" + this.key + "&uri=" + URLEncoder.encode(url) + "&format=txt";
        data = ConnectWeb(temp);
        return data;
    }

    public String ConnectWeb(String long_url) {
        String short_url = "";
        try {
            URL url = new URL(long_url);
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
            short_url = in.readLine();
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return short_url;
        }
    }
    
    public static void main(String[] args) {
    	AcortaUrl o = new AcortaUrl();
    	try {
			System.out.println(o.Shorten("http://multasradar.appspot.com/web/pages/signin/index.html"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
