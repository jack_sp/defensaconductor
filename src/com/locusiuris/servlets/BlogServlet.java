package com.locusiuris.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.datanucleus.exceptions.NucleusObjectNotFoundException;

import com.google.appengine.api.datastore.Text;
import com.locuiuris.vo.EntradaWebVO;
import com.locusiuris.db.BlogPostDB;
import com.locusiuris.utils.StringKeys;

@SuppressWarnings("serial")
public class BlogServlet extends HttpServlet {
	
	private List<EntradaWebVO> getEntradas(BlogPostDB oBlogPostDB){
		List<EntradaWebVO> oListEntradas = oBlogPostDB.listadoPost();
		List<EntradaWebVO> oListSession = new ArrayList<EntradaWebVO>();
		for (EntradaWebVO oEntradaVO :oListEntradas){
			oListSession.add(oEntradaVO);
		}
		return oListSession;
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		String sCadena = (String) req.getParameter(StringKeys.OPERACION);
		String sSitioWeb = "backoffice";
		try{
		if (((String) req.getParameter(StringKeys.SITIOWEB)).equals("1"))
			sSitioWeb = "web";
		}catch(Exception e){
			
		}
		BlogPostDB oBlogPostDB = new BlogPostDB();
		String sUrl = "./blog.jsp";
		
		if (sCadena.equals(StringKeys.LISTAENTRADAS)){			
			List<EntradaWebVO> oListSession  = getEntradas(oBlogPostDB);
			
			try{
				if (((String) req.getParameter(StringKeys.DESDEINDEX)).equals("1"))
					sUrl = "./web/pages/"+sSitioWeb+"/index.jsp";
			}catch(Exception e){}			
			req.getSession().setAttribute("listaentradas", oListSession);
		}else if (sCadena.equals(StringKeys.BORRADO)){
			String sKey = (String) req.getParameter(StringKeys.KEY);
			oBlogPostDB.borradoEntrada(sKey);
			
			List<EntradaWebVO> oListSession  = getEntradas(oBlogPostDB);
			req.getSession().setAttribute("listaentradas", oListSession);
			
		}else if (sCadena.equals(StringKeys.EDITADO)){
			String sKey = (String) req.getParameter(StringKeys.KEY);
			EntradaWebVO oEntradaVO =oBlogPostDB.consultaEntrada(sKey);
			
			req.getSession().setAttribute("EntradaVO", oEntradaVO);
			sUrl =	"./web/pages/backoffice/editEntrada.jsp?key="+req.getParameter("key");
		}else if (sCadena.equals(StringKeys.DETALLEENTRADA)){
			String sKey = (String) req.getParameter(StringKeys.KEY);
			EntradaWebVO oEntradaVO = new EntradaWebVO();
			try{
				oEntradaVO =oBlogPostDB.consultaEntrada(sKey);
				req.getSession().setAttribute("EntradaVO", oEntradaVO);
				sUrl =	"./detalleEntrada.jsp?key="+req.getParameter("key");
			}catch(javax.jdo.JDOObjectNotFoundException e){
				oEntradaVO = new EntradaWebVO();
				sUrl =	"./web/pages/web/paginanoencontrada.html";
			}			
		}else if (sCadena.equals(StringKeys.ADDENTRADA)){
			List<String> oListaCategorias  =oBlogPostDB.listadoCategorias();
			
			req.getSession().setAttribute((String)StringKeys.LISTACATEGORIAS, oListaCategorias);
			sUrl =	"./web/pages/backoffice/addentrada.jsp";
		}
		
		resp.sendRedirect(sUrl);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String sCadena = (String) req.getParameter(StringKeys.OPERACION);
		BlogPostDB oBlogPostDB = new BlogPostDB();
		EntradaWebVO oEntradaVO = new EntradaWebVO();
		oEntradaVO.setsContenido(new Text((String) req.getParameter(StringKeys.NOTATEXTAREA)));
		oEntradaVO.setsResumen((String) req.getParameter(StringKeys.RESUMEN));
		oEntradaVO.setsTitulo((String) req.getParameter(StringKeys.TITULO));
		if (((String) req.getParameter(""+StringKeys.NUEVACATEGORIA))==null){
			oEntradaVO.setsServicio(((String) req.getParameter(""+StringKeys.SERVICIO)));
		}else{
			oEntradaVO.setsServicio(((String) req.getParameter(""+StringKeys.NUEVACATEGORIA)));
		}
		String sUrl = "./web/pages/backoffice/blog.jsp";
		
		if (sCadena.equals(StringKeys.ALTAPOST)){
			oBlogPostDB.guardarPost(oEntradaVO);
		}else if (sCadena.equals(StringKeys.EDITARPOST)){
			String sId = (String) req.getParameter(StringKeys.KEY);
			oBlogPostDB.actualizaEntrada(sId,oEntradaVO);
		}
		List<EntradaWebVO> oListSession  = getEntradas(oBlogPostDB);
		req.getSession().setAttribute("listaentradas", oListSession);
		
		resp.sendRedirect(sUrl);
	}
	
}
