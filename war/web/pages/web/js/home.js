$(document).ready(function() {

var map;
function initialize() {
  var mapOptions = {
    zoom: 14,
    center: new google.maps.LatLng(43.3615909,-5.8452026),
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  map = new google.maps.Map(document.getElementById('map-canvas'),
                            mapOptions);
  
  var marker = new google.maps.Marker({
      position: new google.maps.LatLng(43.3615909,-5.8452026),
      map: map,
      title: 'Alvarez abogado'
  });
  
                                                                              
                                                                                  
  google.maps.event.addListener(marker, 'click', function() {                
      infowindow.open(map, marker);                                         
  });   
  
  
  
}

google.maps.event.addDomListener(window, 'load', initialize);

});