<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html lang="es">
<head>
  <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="DC.Title" content="Abogados Oviedo" />
	<meta http-equiv="title" content="Abogados Oviedo" />
	<meta http-equiv="description" content="Despacho de abogados en Oviedo" />
	<meta http-equiv="DC.Description" content="Despacho de abogados en Oviedo" />
	<meta name="keywords" content="abogados, oviedo, Villaviciosa de Asturias, abogados laboralistas, multas, tráfico, trafico" />
	<meta name="REVISIT" content="2 days" />
	<meta name="REVISIT-AFTER" content="2 days" />
	<meta name="audience" content="Alle" />
	<meta name="classification" content="web" />
	<meta name="rating" content="general"/> 
	<meta name="Robots" content="Index, Follow" />
	<meta name="language" content="ES" />
	<meta name="DC.Language" scheme="RFC1766" content="Spanish" />
	<meta name="VW96.objecttype" content="Document" />
	<meta name="distribution" content="global" />
	<meta name="resource-type" content="document" />
	<meta http-equiv="Last-Modified" content="0" />
	<meta http-equiv="Cache-Control" content="no-cache, must-revalidate" />
	<meta http-equiv="Content-Language" content="es" />
	<meta name="author" content="Alberto Marqués">
<link rel="shortcut icon" href="../../assets/ico/favicon.png">

<title>ALVAREZ ABOGADOS</title>

<link href="../../dist/css/bootstrap.css" rel="stylesheet">
<link href="../../dist/css/bootstrap-theme.css" rel="stylesheet">
<link href="./css/locusweb.css" rel="stylesheet">
<link href="./css/carousel.css" rel="stylesheet">

<style type="text/css">
html {
	height: 100%
}

body {
	height: 100%;
	margin: 0;
	padding: 0;
}

h3 {
	color: #ffcc33;
}
h2 {
	color: #ffcc33;
}
</style>
<!-- Custom styles for this template -->
<link href="./css/offcanvas.css" rel="stylesheet">
<script src="../../assets/js/html5shiv.js"></script>
<script src="../../assets/js/respond.min.js"></script>
<script src="js/jquery.js"></script>
<script type="text/javascript"
	src="http://maps.googleapis.com/maps/api/js?sensor=false">
	
</script>
<script type="text/javascript" src="js/home.js"></script>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->

</head>

<body>
	<div id="fb-root"></div>
	<script src="./js/facebook.js"></script>
	<div class="navbar navbar-fixed-top" role="navigation">
		<div class="navbar-inner">
			<div class="container alturamenu">
				<div class="row alturamenu1">
					<div class=".col-xs-6 .col-lg-4"></div>
					<div class=".col-xs-12 .col-lg-8 ">
						<div class="sf-menu collapse navbar-collapse ">
							<br>
							<ul class="nav navbar-nav ">
								<li ><a href="../../../welcome">INICIO</a></li>
								<li class="active"><a href="quien.jsp">QUIENES SOMOS</a></li>
								<li><a
									href="../../../servicioservlet?operacion=<%=com.locusiuris.utils.StringKeys.LISTASERVICIOS%>&sitioweb=1">SERVICIOS</a></li>
								<li><a href="consulta.jsp">CONSULTAS</a></li>
								<li><a
									href="../../../blogservlet?operacion=<%=com.locusiuris.utils.StringKeys.LISTAENTRADAS%>&sitioweb=1">BLOG</a></li>
								<li><a href="donde.jsp">DONDE ESTAMOS</a></li>
								<li>
								<div class="fb-like" layout="button_count"
										data-href="https://www.facebook.com/pages/Alvarezabogadosdespacho/526673600750646"
										data-width="100" data-show-faces="false" data-send="false" style="margin-top: 16px;"></div>&nbsp;<a href="https://twitter.com/alvarezdespacho"
									class="twitter-follow-button" data-show-count="true"
									data-lang="es" data-dnt="false"  >Seguir</a></li>
								<li><br>
								<div style="margin-top: 1px;">
								</div></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-md-4 col-lg-3">
						<div class="navbar-header">
						<a class="navbar-brand" href="index.jsp"><img src="../../img/logo.png" style="height: 50px;"/></a>
							<button type="button" class="btn-custom navbar-toggle"
								data-toggle="collapse" data-target=".navbar-collapse">
								<span>Menu</span>
							</button>
							
						</div>
					</div>
					<div class="hidden-xs col-md-4 col-lg-5 "><br><div class="tel"><p><img alt="" src="./img/icon_mail.png" class="iconocontacto" >&nbsp;abogadosagb@gmail.com</p></div> </div>
					<div class="hidden-xs col-md-4 col-lg-4 "><br><div class="tel"><p><img alt="" src="./img/telefono.png" class="iconocontacto" >&nbsp;984.127.087</p></div> </div>
				</div>
			</div>
		</div>
	</div>
	<!-- /.navbar -->

	<div class="container">

		<div class="row row-offcanvas row-offcanvas-right">
			<div class="col-xs-12 col-sm-9">

				<br> <br>

				<div class="row">
					<div class="col-xs-12 col-sm-8 col-lg-8"></div>
					<div class="col-xs-6 col-sm-4 col-lg-4 textocapitulo">
						
					</div>
				</div>


				<div class="row">
					<div class="col-xs-12 col-sm-8 col-lg-8">
						<br><h3>Nuestro Despacho</h3><br>
						<p>Con más de veinte años de experiencia, somos un despacho de abogados ubicado 
						en el Oviedo Antiguo, calle Altamirano enfrente de la antigua Facultad de Derecho 						
						y al lado del parking de La Escandalera y del parking de El Vasco. Nuestra forma 						
						de entender el Derecho va indiscutiblemente unida al lugar donde hemos situado el 						
						despacho, tradicional pero a la vez actual, multicultural y comprometido.						
						En el actual contexto de crisis económica hay que valorar mas que nunca una serie de 						
						habilidades y empatias desdeñadas por las grandes corporaciones jurídicas.</p>
						<p>
						En la actualidad podemos ofrecer un soporte estructural enfocado a la multidisciplina, 						
						desarrollando nuestro trabajo en ámbitos como el Derecho Civil, Penal, Administrativo 						
						–con especial interés referido a extranjería, sanciones de trafico y seguridad vial y 						
						transporte- y laboral.						
						La atención personalizada con continua comunicación con el defendido, la 						
						transparencia y asistencia integral, unido a unos honorarios contenidos y ajustados, 						
						conforman el sello de identidad de este despacho profesional.</p>
						<br><h3>Equipo</h3><br>
						<p>Formado por profesionales del derecho especializados y con una larga trayectoria 
						profesional en las distintas áreas jurídicas de trabajo del despacho.						
						Colaboran con nosotros diversos especialistas tales como Procuradores, Médicos 						
						Especialistas, Ingenieros, Arquitectos, que a la hora de ejecutar los distintos trabajos 						
						y/o elaborar informes resultan muchas veces trascendentales a la hora de decidir la 						
						resolución de un asunto.</p>
					</div>
					<div class="col-xs-6 col-sm-4 col-lg-4 ">
						<div class="imgcapitulo">
				
							<img src="../../img/derecho.jpg" style="width: 325px;">
						</div>
					</div>
				</div>


			</div>
			<!--/span-->
			<br> <br>

		</div>
		<!--/row-->

		<hr>

	<jsp:include page="./includes/footer.jsp" />

	</div>
	<!--/.container-->


	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="../../assets/js/jquery.js"></script>
	<script src="../../dist/js/bootstrap.min.js"></script>
	<script src="offcanvas.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-44892985-1', 'alvarezdespacho.appspot.com');
  ga('send', 'pageview');

</script>
</body>
</html>
