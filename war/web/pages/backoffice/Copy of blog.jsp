<%@page import="com.locusiuris.utils.StringKeys"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"    pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/displaytag-11.tld" prefix="display" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.png">

    <title>BackOffice Alvarez Abogados</title>

    <!-- Bootstrap core CSS -->
    <link href="../../dist/css/bootstrap.css" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="../../dist/css/displaytag.css"/>

    <!-- Custom styles for this template -->
    <link href="navbar.css" rel="stylesheet">
<style type="text/css">
html {
	height: 100%
}

body {
	height: 100%;
	margin: 0;
	padding: 0;
}

h3 {
	color: #ff9999;
}

/*	  html, body, #map-canvas {
			  margin: 0;
			  padding: 0;
			  height: 100%;
			}
			
			#map-canvas {
			  width:600px
			}*/
</style>


  </head>

  <body>

    <div class="container">

      <!-- Static navbar -->
      <div class="navbar navbar-default">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Back Office</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="./index.jsp">Blog</a></li>
            <li><a href="./servicios.jsp">Servicios</a></li>
            <li><a href="#">Carrusel</a></li>
            <li><a href="#">Configuración</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li class="dropdown-header">Nav header</li>
                <li><a href="#">Separated link</a></li>
                <li><a href="#">One more separated link</a></li>
              </ul>
            </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>

      <!-- Main component for a primary marketing message or call to action -->
      <div class="jumbotron">
        
        <p>
          <a class="btn btn-lg btn-primary" href="../../../blogservlet?operacion=<%=StringKeys.ADDENTRADA%>">Nueva entrada blog</a>
        </p>
        <p>
          <a class="btn btn-lg btn-primary" href="../../../blogservlet?operacion=<%=StringKeys.LISTAENTRADAS%>">Ver entradas</a>
        </p>
        <display:table  name="sessionScope.listaentradas" pagesize="10" requestURI="../../../blogservlet?operacion=listado">
		    <display:column property="sTitulo" title="Titulo" />
		    <display:column property="sServicio" title="Categoría"  />
		    <display:column property="oDateFechaEntrada"  />
		<display:column property="key"  />
		    <display:column title="Borrar" href="../../../blogservlet?operacion=borrado" paramId="key" paramProperty="key">
					 Borrar
				</display:column>
			<display:column title="Editar" href="../../../blogservlet?operacion=editado" paramId="key" paramProperty="key">
				 Editar
			</display:column>
		  </display:table>
      </div>

    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../../assets/js/jquery.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>
  </body>
</html>