<%@page import="java.util.ArrayList"%>
<%@page import="com.locuiuris.vo.EntradaWebVO"%>
<%@page import="java.util.List"%>
<%@page import="com.locusiuris.utils.StringKeys"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="DC.Title" content="Abogados Oviedo" />
	<meta http-equiv="title" content="Abogados Oviedo" />
	<meta http-equiv="description" content="Despacho de abogados en Oviedo" />
	<meta http-equiv="DC.Description" content="Despacho de abogados en Oviedo" />
	<meta name="keywords" content="abogados, oviedo, Villaviciosa de Asturias, abogados laboralistas, multas, tráfico, trafico" />
	<meta name="REVISIT" content="2 days" />
	<meta name="REVISIT-AFTER" content="2 days" />
	<meta name="audience" content="Alle" />
	<meta name="classification" content="web" />
	<meta name="rating" content="general"/> 
	<meta name="Robots" content="Index, Follow" />
	<meta name="language" content="ES" />
	<meta name="DC.Language" scheme="RFC1766" content="Spanish" />
	<meta name="VW96.objecttype" content="Document" />
	<meta name="distribution" content="global" />
	<meta name="resource-type" content="document" />
	<meta http-equiv="Last-Modified" content="0" />
	<meta http-equiv="Cache-Control" content="no-cache, must-revalidate" />
	<meta http-equiv="Content-Language" content="es" />
	<meta name="author" content="Alberto Marqués">
<link rel="shortcut icon" href="../../assets/ico/favicon.png">

<title>DEFENSA DEL CONDUCTOR</title>

<!-- Bootstrap core CSS -->

<link href="./dist/css/bootstrap.css" rel="stylesheet">


<link href="./css/locusweb.css" rel="stylesheet">
<link href="./css/carousel.css" rel="stylesheet">
<style type="text/css">
html {
	height: 100%
}

body {
	height: 100%;
	margin: 0;
	padding: 0
}
h2 {color: #ffcc33;}
      h3 {color: #666666;}
      a{text-decoration: none;color: #666666;}
</style>
<!-- Custom styles for this template -->
<link href="./css/offcanvas.css" rel="stylesheet">
<script src="../../assets/js/html5shiv.js"></script>
<script src="../../assets/js/respond.min.js"></script>
<!-- paginador -->

<!-- paginador -->

<script src="js/jquery.js"></script>
<script type="text/javascript"
	src="http://maps.googleapis.com/maps/api/js?sensor=false">
	
</script>
<script type="text/javascript" src="js/home.js"></script>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->

</head>

<body>
	<div id="fb-root"></div>
	<script src="./js/facebook.js"></script>
	<jsp:include page="./includes/header.jsp" />
	<!-- /.navbar -->
	<div class="container">
		<br>   <br> 
    <!--    <div class="row"> -->



			<div class="row">
				<div class="col-xs-6 col-sm-4 col-lg-4">
				
							<!-- Text input-->
							<div class="control-group">
								<label class="control-label"></label>
								<div class="controls">
									<form class="form-horizontal" action="./blog.jsp" method="get" >
										<input id="buscar" name="buscar" type="text"
											placeholder="Busqueda *" class="input-xlarge" required>
										<input type="submit" class="btn btn-info btn-small active" value="Buscar" style="margin-top: 20px;margin-bottom: 20px;"/>
										
									</form>
								</div>
							</div>

						
				
				</div>
				<div class="col-xs-12 col-sm-8 col-lg-8 textocapitulo">
					<h2>Blog</h2>
				</div>
			</div>

			<% //buscador del blog
				List<EntradaWebVO> oListEntradas = null;
					List<String> oListCategoria = new ArrayList<String>();
					String sVerTodos="";
					String listaCategorias ="";
					if (request.getSession().getAttribute(StringKeys.LISTAENTRADAS) != null){
						oListEntradas = (List<EntradaWebVO>) request.getSession().getAttribute(StringKeys.LISTAENTRADAS);
					
						if (request.getParameter("buscar")!=null){
							String sCadenaBusqueda = request.getParameter("buscar");
							List<EntradaWebVO> oListBusquedas = new ArrayList();
							
							for (EntradaWebVO entrada : oListEntradas){
								if (entrada.getsTitulo().contains(sCadenaBusqueda)||(""+entrada.getsContenido()).contains(sCadenaBusqueda))
									oListBusquedas.add(entrada);
							}
							oListEntradas = oListBusquedas; 
							sVerTodos = "<a href=\"../../../blogservlet?operacion="+com.locusiuris.utils.StringKeys.LISTAENTRADAS+"&sitioweb=1\" >&laquo;<i>Volver al blog</i></a>";
						}
						if (request.getParameter("buscarCategoria")!=null){
							String sCadenaBusqueda = request.getParameter("buscarCategoria");
							List<EntradaWebVO> oListBusquedas = new ArrayList();
							
							for (EntradaWebVO entrada : oListEntradas){
								if (entrada.getsServicio().contains(sCadenaBusqueda))
									oListBusquedas.add(entrada);
							}
							for(EntradaWebVO entrada:oListEntradas){
								if (!oListCategoria.contains(entrada.getsServicio()))
									oListCategoria.add(entrada.getsServicio());
							}
							oListEntradas = oListBusquedas; 
							for(String categoria:oListCategoria){
								listaCategorias = listaCategorias+"<li><a href=\"blog.jsp?buscarCategoria="+categoria+"\" active><i class=\"icon-edit\">&gt;&gt;</i>"+ categoria+"</a></li>"; 
							}
							sVerTodos = "<a href=\"../../../blogservlet?operacion="+com.locusiuris.utils.StringKeys.LISTAENTRADAS+"&sitioweb=1\" >&laquo;<i>Volver al blog</i></a>";
						}
						
						
						
					}
					int i = 0;
					if(oListEntradas!=null){
						for(EntradaWebVO entrada:oListEntradas){
							if (!oListCategoria.contains(entrada.getsServicio()))
								oListCategoria.add(entrada.getsServicio());
						}
						if (listaCategorias.equals(""))
							for(String categoria:oListCategoria){
								listaCategorias = listaCategorias+"<li><a href=\"blog.jsp?buscarCategoria="+categoria+"\"><i class=\"icon-edit\">&gt;&gt;</i>"+ categoria+"</a></li>"; 
							}
						
						String sHtml = "";
						String sMenu = " <div class=\"row\">"
								+ "<div class=\"col-xs-6 col-sm-4 col-lg-4 \" >"
								+
	
								"<div class=\"well sidebar-nav cajabuscador2\" style=\"width:200px; padding: 8px 0;\">"
								+"<ul class=\"nav nav-list\"> "						
								+ "<li class=\"nav-header\" ><b><a href=\"./blog.jsp\">CATEGORIAS</a></b></li>"
								
								+ listaCategorias
								
								+ "</ul>" + "" + "</div></div>"
								+ "<div class=\"col-xs-12 col-sm-8 col-lg-8\">"
								+ "<h2>Titulo</h2>" + " <h3>FechaEntrada</h3>"
								+ "<p>Contenido</p></div>" +
	
								" </div>";
						String sEntrada = "<div class=\"row\">"
								+ "<div class=\"col-xs-6 col-sm-4 col-lg-4 \" >&nbsp;</div><div class=\"col-xs-12 col-sm-8 col-lg-8\">"
								+ "<h2>Titulo</h2><h3>FechaEntrada</h3><p>Contenido</p></div></div>";
	
						String patron = "EEE, d MMM yyyy";
						java.text.SimpleDateFormat formato = new java.text.SimpleDateFormat(
								patron);
						if(oListEntradas!=null){
							for (EntradaWebVO entrada : oListEntradas) {
								if (i == 0) {
									sMenu = sMenu.replace("Titulo", "<a href=\"../../../blogservlet?operacion="+StringKeys.DETALLEENTRADA+"&key="+entrada.getKey()+"\">"+entrada.getsTitulo()+"</a>");
									sMenu = sMenu
											.replace(
													"FechaEntrada",
													""
															+ formato.format(entrada
																	.getoDateFechaEntrada()));
									sMenu = sMenu.replace("Contenido",
											"" + entrada.getsContenido());
									sHtml = sHtml + sMenu;
								} else {
									sEntrada = sEntrada.replace("Titulo", "<a href=\"../../../blogservlet?operacion="+StringKeys.DETALLEENTRADA+"&key="+entrada.getKey()+"\">"+entrada.getsTitulo()+"</a>");
									sEntrada = sEntrada
											.replace(
													"FechaEntrada",
													""
															+ formato.format(entrada
																	.getoDateFechaEntrada()));
									sEntrada = sEntrada.replace("Contenido",
											"" + entrada.getsContenido());
									sHtml = sHtml + sEntrada;
								}
								sEntrada = "<div class=\"row\">"
										+ "<div class=\"col-xs-6 col-sm-4 col-lg-4 \" >&nbsp;</div><div class=\"col-xs-12 col-sm-8 col-lg-8\">"
										+ "<h2>Titulo</h2><h3>FechaEntrada</h3><p>Contenido</p></div></div>";
								i++;
							}
						}
						out.println(sVerTodos+sHtml+sVerTodos);
					}
					
			%>
			
			<div class="row">
				<div class="col-xs-6 col-sm-4 col-lg-4">
				</div>
				<div class="col-xs-12 col-sm-8 col-lg-8">
					<ul class="pagination">
					  <li><a href="#">&laquo;</a></li>
					  <li class="active"><a href="#">1</a></li>
					  <li><a href="#">2</a></li>
					  <li><a href="#">3</a></li>
					  <li><a href="#">4</a></li>
					  <li><a href="#">5</a></li>
					  <li><a href="#">&raquo;</a></li>
					</ul>
				</div>
			</div>



		
		<!--/span-->
		<br> <br>

	
	<!--/row-->

	<hr>

		<jsp:include page="./includes/footer.jsp" />

	</div>
	<!--/.container-->


	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="../../assets/js/jquery.js"></script>
	<script src="../../dist/js/bootstrap.min.js"></script>
	<script src="offcanvas.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-63787202-1', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>
