package com.locuiuris.vo;

import java.util.Date;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;

@PersistenceCapable(detachable = "true")
public class ServicioVO implements java.io.Serializable {

	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		@Persistent
		private String sContenido;
		 @Persistent
		 private String sTitulo;
		 @Persistent
		 private String sServicio;
		 @Persistent
		 private String sResumen;

		 public String getsResumen() {
			return sResumen;
		}
		public void setsResumen(String sResumen) {
			this.sResumen = sResumen;
		}
		public long getKey() {
			return key.getId();
		}
		public void setKey(Key key) {
			this.key = key;
		}
		@PrimaryKey
		    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
		    private Key key;
		 
		 
		@Persistent
		Date oDateFechaEntrada;
		 
		public String getsContenido() {
			return sContenido;
		}
		public void setsContenido(String sContenido) {
			this.sContenido = sContenido;
		}
		public String getsTitulo() {
			return sTitulo;
		}
		public Date getoDateFechaEntrada() {
			return oDateFechaEntrada;
		}
		public void setoDateFechaEntrada(Date oDateFechaEntrada) {
			this.oDateFechaEntrada = oDateFechaEntrada;
		}
		public void setsTitulo(String sTitulo) {
			this.sTitulo = sTitulo;
		}
		public String getsServicio() {
			return sServicio;
		}
		public void setsServicio(String sServicio) {
			this.sServicio = sServicio;
		}

	
}
