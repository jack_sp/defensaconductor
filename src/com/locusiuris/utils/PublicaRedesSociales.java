package com.locusiuris.utils;

import java.util.Date;

import com.restfb.Connection;
import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.Parameter;
import com.restfb.types.FacebookType;
import com.restfb.types.User;

public class PublicaRedesSociales {

	@SuppressWarnings("unused")
	public static void publicaFacebook(String sMensaje) {
		FacebookClient facebookClient = new DefaultFacebookClient(
				StringKeys.TOKENSECURITYFACEBOOK);

		FacebookClient publicOnlyFacebookClient = new DefaultFacebookClient();

		try {
			FacebookType publishMessageResponse = facebookClient.publish(
					StringKeys.IDPUBLICAFACEBOOK, FacebookType.class,
					Parameter.with("title", "titulo"),
					Parameter.with("description", "Descripciones"),
					Parameter.with("start_time", new Date()),
					Parameter.with("message", sMensaje));

			System.out.println("Published message ID: "
					+ publishMessageResponse.getId());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}
	

}
