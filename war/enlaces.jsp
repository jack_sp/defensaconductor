<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html lang="es">
<head>
  <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="DC.Title" content="Abogados Oviedo" />
	<meta http-equiv="title" content="Abogados Oviedo" />
	<meta http-equiv="description" content="Despacho de abogados en Oviedo" />
	<meta http-equiv="DC.Description" content="Despacho de abogados en Oviedo" />
	<meta name="keywords" content="abogados, oviedo, Villaviciosa de Asturias, abogados laboralistas, multas, tráfico, trafico" />
	<meta name="REVISIT" content="2 days" />
	<meta name="REVISIT-AFTER" content="2 days" />
	<meta name="audience" content="Alle" />
	<meta name="classification" content="web" />
	<meta name="rating" content="general"/> 
	<meta name="Robots" content="Index, Follow" />
	<meta name="language" content="ES" />
	<meta name="DC.Language" scheme="RFC1766" content="Spanish" />
	<meta name="VW96.objecttype" content="Document" />
	<meta name="distribution" content="global" />
	<meta name="resource-type" content="document" />
	<meta http-equiv="Last-Modified" content="0" />
	<meta http-equiv="Cache-Control" content="no-cache, must-revalidate" />
	<meta http-equiv="Content-Language" content="es" />
	<meta name="author" content="Alberto Marqués">
<link rel="shortcut icon" href="../../assets/ico/favicon.png">

<title>DEFENSA DEL CONDUCTOR</title>

<link href="../../dist/css/bootstrap.css" rel="stylesheet">
<link href="./css/locusweb.css" rel="stylesheet">
<link href="./css/carousel.css" rel="stylesheet">

<style type="text/css">
html {
	height: 100%
}

body {
	height: 100%;
	margin: 0;
	padding: 0;
}

h3 {
	color: #ffcc33;
}
h2 {
	color: #ffcc33;
}
</style>
<!-- Custom styles for this template -->
<link href="./css/offcanvas.css" rel="stylesheet">
<script src="../../assets/js/html5shiv.js"></script>
<script src="../../assets/js/respond.min.js"></script>
<script src="js/jquery.js"></script>
<script type="text/javascript"
	src="http://maps.googleapis.com/maps/api/js?sensor=false">
	
</script>
<script type="text/javascript" src="js/home.js"></script>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->

</head>

<body>
	<div id="fb-root"></div>
	<script src="./js/facebook.js"></script>
	
		<jsp:include page="./includes/header.jsp" />
	<!-- /.navbar -->

	<div class="container" style="margin-top: 35px;">

		<div class="row row-offcanvas row-offcanvas-right">
			<div class="col-xs-12 col-sm-9">

		

				<div class="row">
					<div class="col-xs-12 col-sm-8 col-lg-8"></div>
					<div class="col-xs-6 col-sm-4 col-lg-4 textocapitulo">
						
					</div>
				</div>


				<div class="row">
					<div class="col-xs-12 col-sm-8 col-lg-8">
						<br><h3>Enlaces Interesantes</h3><br>
		 				
						<div class="list-group">
						
						
						  <a href="#" class="list-group-item active">Enlaces de prensa sobre Defensadelconductor.com<a>
<a href="http://www.lne.es/asturias/2015/03/23/lanzan-aplicacion-movil-defensa-conductor/1731020.html" class="list-group-item" target="_blank">Nueva España</a>
  
   <a href="./prensa/vozdegalicia.pdf" class="list-group-item" target="_blank">La Voz de Galicia</a>
   
						  <a href="#" class="list-group-item active">Enlaces sobre tráfico<a>
<a href="http://www.dgt.es/es/el-trafico/control-de-velocidad/" class="list-group-item" target="_blank">Busca los radares</a>
  <a href="http://www.autoresiduos.com/valor_venal_coche.html" class="list-group-item" target="_blank">Calcular valor venal de su coche o vehículo</a>
   <a href="http://www.dgt.es/es/el-trafico/restricciones" class="list-group-item" target="_blank">Restricciones a la circulación 2015</a>
						
			<a href="#" class="list-group-item active">			Administración Pública<a>
  <a href="http://www.dgt.es" class="list-group-item" target="_blank">DGT</a>
  <a href="http://www.boe.es" class="list-group-item" target="_blank">Boletín Oficial del Estado</a>
  <a href="http://www.interior.gob.es" class="list-group-item" target="_blank">MINISTERIO DEL INTERIOR</a>
  <a href="http://www.empleo.gob.es" class="list-group-item" target="_blank">MINISTERIO DE EMPLEO Y SEGURIDAD SOCIAL</a>
  
   <a href="http://www.mjusticia.gob.es" class="list-group-item" target="_blank">MINISTERIO DE JUSTICIA</a>
  <a href="http://www.fomento.gob.es" class="list-group-item" target="_blank">MINISTERIO DE FOMENTO</a>
  <a href="http://www.asturias.es" class="list-group-item" target="_blank">PRINCIPADO DE ASTURIAS</a>
  <a href="http://www.ine.es" class="list-group-item" target="_blank">INE</a>
  
  
  
  <a href="#" class="list-group-item active">			Ayuntamientos de:</a>
   <a href="http://www.oviedo.es" class="list-group-item" target="_blank">OVIEDO</a>
  <a href="http://www.gijon.es" class="list-group-item" target="_blank">GIJÓN</a>
  <a href="http://www.aviles.es" class="list-group-item" target="_blank">AVILES</a>
  <a href="http://www.coruna.es" class="list-group-item" target="_blank">La Coruña</a>
    <a href="http://www.ourense.es" class="list-group-item" target="_blank" >Orense</a> 
    <a href="http://www.pontevedra.gal" class="list-group-item" target="_blank">Pontevedra</a> 
     <a href="http://lugo.gal" class="list-group-item" target="_blank">Lugo</a> 
         <a href="http://www.ayto-santander.es " class="list-group-item" target="_blank">Santander</a> 
     <a href="http://www.aytoleon.es" class="list-group-item" target="_blank">León</a> 
     
 
						 <a href="#" class="list-group-item active">Internacional<a>
				

  <a href="http://www.mininterior.gov.ar" class="list-group-item" target="_blank">Argentina  : Agencia Nacional de Seguridad Vial </a>
  <a href="http://www.policia.bo" class="list-group-item" target="_blank">Bolivia: Policía Boliviana</a>
  <a href="http://www.denatran.gov.br" class="list-group-item" target="_blank">Brasil: DENATRAN (Departamento Nacional de Tránsito)</a>
  <a href="http://www.conaset.cl" class="list-group-item" target="_blank">Chile: CONASET (Comisión Nacional de Seguridad de Tránsito)</a>
    <a href="http://www.mtt.gob.cl" class="list-group-item" target="_blank" >Ministerio de Transportes y Telecomunicaciones</a> 
    <a href="http://www.mintransporte.gov.co" class="list-group-item" target="_blank">Colombia: Ministerio de Transportes</a> 
     <a href="http://www.csv.go.cr" class="list-group-item" target="_blank">Costa Rica: COSEVI (Consejo de Seguridad Vial)</a> 
         <a href="http://www.cubagob.cu" class="list-group-item" target="_blank">Cuba: CONASET (Comisión Nacional de Seguridad de Tránsito)</a> 
     <a href="http://www.ant.gob.ec" class="list-group-item" target="_blank">Ecuador: Agencia Nacional de Tránsito</a> 
  <a href="http:// www.mop.gob.sv " class="list-group-item" target="_blank"> El Salvador: Ministerio de Transportes 
</a>

  <a href="http://www.mingob.gob.gt/" class="list-group-item" target="_blank"> Guatemala: Departamento de Tránsito Dirección Nacional de la Policía Nacional
</a>

  <a href="http://www.direccionnacionaldetransito.gob.hn" class="list-group-item" target="_blank"> Honduras: Dirección de Tránsito de la Policía </a>


<a href="http://www.cenapra.salud.gob.mx/" class="list-group-item" target="_blank"> México: Cenapra (Consejo Nacional de Prevención de Accidentes - Secretaría de Salud) </a>


<a href="http://www.policia.gob.ni" class="list-group-item" target="_blank"> Nicaragua: Dirección de Tránsito de la Policía Nacional </a>


<a href="http://www.transito.gob.pa" class="list-group-item" target="_blank"> Panamá: Autoridad de Tránsito y Transportes Terrestres </a>

 <a href="http://www.mopc.gov.py" class="list-group-item" target="_blank"> Paraguay: CNSV (Consejo Nacional de Seguridad Vial) </a>

  <a href="http://www.mtc.gob.pe" class="list-group-item" target="_blank"> Perú: Consejo Nacional de Seguridad Vial </a>

  <a href="http://www.fondet.gob.do" class="list-group-item" target="_blank"> República Dominicana: FONDET (Fundación para la Formación de Conductores) </a>

 <a href="http://www.presidencia.gub.uy" class="list-group-item" target="_blank"> Uruguay: UNASEV (Unidad Nacional de Seguridad Vial) </a>

<a href="http://www.inttt.gov" class="list-group-item" target="_blank"> Venezuela: INTT (Instituto Nacional de Transporte y Tránsito) </a>



</div>
					</div>
					<div class="col-xs-6 col-sm-4 col-lg-4 ">
						<div class="imgcapitulo">
				
							<img src="../../img/enlaces.jpg" style="width: 325px;">
						</div>
					</div>
				</div>


			</div>
			<!--/span-->
			<br> <br>

		</div>
		<!--/row-->

		<hr>

	<jsp:include page="./includes/footer.jsp" />

	</div>
	<!--/.container-->


	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="../../assets/js/jquery.js"></script>
	<script src="../../dist/js/bootstrap.min.js"></script>
	<script src="offcanvas.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-63787202-1', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>
