package com.locusiuris.utils;

public class StringKeys {

	public static final String IDENTRADA = "identrada";
	public static final String OPERACION = "operacion";	
	public static final String ALTAPOST = "altapost";
	public static final String EDITARPOST = "editarpost";
	public static final String ALTASERVICIO = "altaservicio";
	public static final String EDITARSERVICIO = "editarservicio";
	public static final String BORRADO = "borrado";
	public static final String EDITADO = "editado";
	public static final String DETALLESERVICIO = "detalleservicio";
	public static final String DETALLEENTRADA = "detalleentrada";	
	public static final String CONTENIDO = "editor";
	public static final String TITULO = "titulo";
	public static final String SERVICIO = "servicio";
	public static final String KEY = "key";
	public static final String LISTASERVICIOS = "listaservicios";
	public static final String LISTAENTRADAS = "listaentradas";	
	public static final String ENTRADAVO = "EntradaVO";
	public static final String SERVICIOVO = "EntradaVO";
	public static final String SITIOWEB = "sitioweb";
	public static final String NOTATEXTAREA =  "notatextarea";
	public static final String DESDEINDEX = "desdeindex";
	public static final String RESUMEN = "resumen";
	public static final Object ADDENTRADA = "addentrada";
	public static final Object LISTACATEGORIAS = "listacategorias";
	public static final Object DESACTIVADO = "desactivado";
	public static final Object NUEVACATEGORIA = "nuevaCategoria";
	public static final String IDPUBLICAFACEBOOK = "526673600750646/feed";
	public static final String TOKENSECURITYFACEBOOK = "CAAUQzZCzpNbABAGgdZCDmxUPfiwhhVZBkL0ZAElkSg2odddrA4KzKqE56pr4Uurb4cEV4t5mb9lCHG0FxY0uwmXHqr8ydQ6ZBpnqavjwrriAA6Y2rMlZAQMoc2b4w9cZAUX15WcEDfBrBQ7LODE2RDlFe9v2jinX2qZAFG75qgCv1lGZAF8RP1HCT";
	public static final String USERBIT = "alvarezabogados";
	public static final String PASSWORD = "R_8c36dfd0fc3d3d4e4677434b05957313";
}
