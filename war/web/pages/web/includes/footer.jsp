<footer>

			<div class="row"  style='margin-top:80px;'>
				<div class="col-6 col-sm-6 col-lg-4">
					<h4>Contacto</h4>
					<p>
						Tel&eacute;fono: 984.187.027<br>
					</p>
					<p>abogadosagb@gmail.com</p>
					<h4>Localizaci&oacute;n</h4>
					<p>
						C/ Altamirano 3,<br>33003, Oviedo <br>Espa�a
					</p>
				</div>
				<!--/span-->
				<div class="col-6 col-sm-6 col-lg-4">
				<br><br>
					<div id="map-canvas" class="mapas"
						style="width: 250px; height: 150px;"></div>
				</div>
				<!--/span-->
				<div class="col-6 col-sm-6 col-lg-4">
					
					<div class="social-links">
					<h4>S�guenos</h4>
					<ul>
					<li class="twitter"><a href="https://twitter.com/alvarezdespacho" title="Twitter" target="_blank">Twitter</a></li>
					<li class="facebook"><a href="https://www.facebook.com/pages/Alvarezabogadosdespacho/526673600750646" title="facebook" target="_blank">Facebook</a>
					</li>
					<li class="linkedin"><a href="http://www.linkedin.com/" title="LinkedIn" target="_blank">LinkedIn</a></li>
					
					</div>
					<p>
						
						<script>
							!function(d, s, id) {
								var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/
										.test(d.location) ? 'http' : 'https';
								if (!d.getElementById(id)) {
									js = d.createElement(s);
									js.id = id;
									js.src = p
											+ '://platform.twitter.com/widgets.js';
									fjs.parentNode.insertBefore(js, fjs);
								}
							}(document, 'script', 'twitter-wjs');
						</script>
					</p>
					<h4>Localizaci&oacute;n</h4>
					<p>
						Avda. Principado 20, 6� J,<br>33404. Corvera de Asturias <br>Espa�a
					</p>
					
					<p>
						Plaza del Ayuntamiento 20, 1� izda.,<br>33300. Villaviciosa de Asturias<br>Espa�a
					</p>
				</div>
				<!--/span-->
			</div>
			<!--/row-->
			<hr>
			<p>&copy; Alvarez Abogado 2013</p>
		</footer>