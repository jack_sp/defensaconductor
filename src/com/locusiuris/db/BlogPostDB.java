package com.locusiuris.db;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.jdo.PersistenceManager;

import com.google.appengine.api.datastore.Text;
import com.locuiuris.vo.EntradaWebVO;
import com.locusiuris.utils.AcortaUrl;
import com.locusiuris.utils.PublicaRedesSociales;
import com.serverrest.jdo.PMF;

public class BlogPostDB {
	
	
	

	public void guardarPost(EntradaWebVO oEntradaVO) {
		 PersistenceManager pm = PMF.get().getPersistenceManager();
	        try {
	        	oEntradaVO.setoDateFechaEntrada(new Date());
	        	pm.makePersistent(oEntradaVO);
	        	String url = (new AcortaUrl()).Shorten("http://alvarezdespacho.appspot.com/blogservlet?operacion=detalleentrada&key="+oEntradaVO.getKey());
	        	PublicaRedesSociales.publicaFacebook(oEntradaVO.getsResumen()+" "+url);
	            
	        } catch (IOException e) {
				e.printStackTrace();
			} finally {
	            pm.close();
	        }
	}
	
	public List<EntradaWebVO> listadoPost() {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		javax.jdo.Query query = pm.newQuery(EntradaWebVO.class);
		query.setOrdering("oDateFechaEntrada desc");
      //  query.setFilter( "id == idSpot" );
       // query.declareParameters( "String idSpot" );
		
        return(List<EntradaWebVO>) query.execute(query) ; 
	}

	public void borradoEntrada(String id) {
		PersistenceManager pm = PMF.get().getPersistenceManager();
        try {
        	EntradaWebVO oEntradaVO = pm.getObjectById(EntradaWebVO.class, (new Double(id)).longValue());
            pm.deletePersistent(oEntradaVO);
        } finally {
            pm.close();
        }
	}

	public void actualizaEntrada(String id,EntradaWebVO oEntrada1) {
		 PersistenceManager pm = PMF.get().getPersistenceManager();
		    try {
	        	EntradaWebVO oEntradaVO = pm.getObjectById(EntradaWebVO.class, (new Double(id)).longValue());
	        	oEntradaVO.setsTitulo(oEntrada1.getsTitulo());
	        	oEntradaVO.setsContenido(new Text(oEntrada1.getsContenido()));
	        	oEntradaVO.setsServicio(oEntrada1.getsServicio());
	        	oEntradaVO.setsResumen(oEntrada1.getsResumen());
	        	oEntradaVO.setoDateFechaEntrada(new Date());
	        } finally {
	            pm.close();
	        }
	}
	
	public EntradaWebVO consultaEntrada(String id) {
		 PersistenceManager pm = PMF.get().getPersistenceManager();
		 EntradaWebVO oEntradaVO = null;
		    try {
	        	oEntradaVO = pm.getObjectById(EntradaWebVO.class, (new Double(id)).longValue());
	        } finally {
	            pm.close();
	        }
		 return oEntradaVO;
	}

	public List<String> listadoCategorias() {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		javax.jdo.Query query = pm.newQuery(EntradaWebVO.class);

		List<String> oListCategorias = new ArrayList<String>();
		List<EntradaWebVO> oListEntradaVO = (List<EntradaWebVO>) query.execute(query);
		
		for(EntradaWebVO entrada: oListEntradaVO){
			if (!oListCategorias.contains(entrada.getsServicio()))
			oListCategorias.add(entrada.getsServicio());
		}
		
        return oListCategorias; 
	}

	

}
